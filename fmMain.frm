VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form fmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   8295
   ClientLeft      =   150
   ClientTop       =   780
   ClientWidth     =   11595
   Icon            =   "fmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8295
   ScaleWidth      =   11595
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog dlgFile 
      Left            =   10920
      Top             =   5640
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame frmRadio 
      Caption         =   "frmRadio"
      Height          =   1695
      Left            =   7320
      TabIndex        =   49
      Top             =   4560
      Width           =   3135
      Begin VB.CommandButton cmdWAV 
         Caption         =   "WAV"
         Height          =   315
         Left            =   480
         TabIndex        =   52
         Top             =   1200
         Width           =   750
      End
      Begin VB.CommandButton cmdClearRadio 
         Caption         =   "Clear"
         Height          =   315
         Left            =   1320
         TabIndex        =   53
         Top             =   1200
         Width           =   750
      End
      Begin VB.CommandButton cmdTransmit 
         Caption         =   "PTT"
         Height          =   315
         Left            =   2160
         TabIndex        =   51
         Top             =   1200
         Width           =   750
      End
      Begin VB.TextBox txtTransmit 
         Height          =   375
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   50
         Text            =   "fmMain.frx":0442
         Top             =   600
         Width           =   2895
      End
      Begin VB.Label lblRadioUnit 
         Caption         =   "lblRadioUnit"
         Height          =   255
         Left            =   1320
         TabIndex        =   55
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label24 
         Caption         =   "Selected Unit:"
         Height          =   255
         Left            =   120
         TabIndex        =   54
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame frmFilterActive 
      Caption         =   "frmFilterActive"
      Height          =   3735
      Left            =   3180
      TabIndex        =   38
      Top             =   1320
      Width           =   2955
      Begin VB.CheckBox chkOffDuty 
         Caption         =   "Show OffDuty Units"
         Height          =   255
         Left            =   60
         TabIndex        =   46
         Top             =   840
         Width           =   2595
      End
      Begin VB.CommandButton cmdClearActiveFilter 
         Caption         =   "Clear Filter"
         Height          =   315
         Left            =   1500
         TabIndex        =   45
         Top             =   3000
         Width           =   1335
      End
      Begin VB.CommandButton cmdSetActiveFilter 
         Caption         =   "Set Filter"
         Height          =   315
         Left            =   60
         TabIndex        =   44
         Top             =   3000
         Width           =   1395
      End
      Begin VB.ListBox lstActiveDivisions 
         Height          =   450
         Left            =   60
         MultiSelect     =   2  'Extended
         TabIndex        =   43
         Top             =   2460
         Width           =   2775
      End
      Begin VB.CheckBox chkActiveDivision 
         Caption         =   "Include All Divisions"
         Height          =   195
         Left            =   60
         TabIndex        =   42
         Top             =   2220
         Width           =   2655
      End
      Begin VB.ListBox lstActiveJurisdictions 
         Height          =   450
         Left            =   60
         MultiSelect     =   2  'Extended
         TabIndex        =   41
         Top             =   1680
         Width           =   2775
      End
      Begin VB.CheckBox chkActiveJurisdiction 
         Caption         =   "Include All Jurisdictions"
         Height          =   195
         Left            =   60
         TabIndex        =   40
         Top             =   1440
         Width           =   2535
      End
      Begin VB.ComboBox cmbActiveAgency 
         Height          =   315
         Left            =   60
         TabIndex        =   39
         Text            =   "cmbActiveAgency"
         Top             =   480
         Width           =   2715
      End
      Begin VB.Label Label1 
         Caption         =   "Select Agency"
         Height          =   195
         Left            =   60
         TabIndex        =   47
         Top             =   240
         Width           =   1755
      End
   End
   Begin VB.Timer tmTimer 
      Left            =   11100
      Top             =   2880
   End
   Begin VB.Frame frmANIALIControl 
      Caption         =   "frmANIALIControl"
      Height          =   1335
      Left            =   5040
      TabIndex        =   31
      Top             =   5100
      Width           =   3135
      Begin VB.CommandButton cmdSendCellular 
         Caption         =   "Cell"
         Height          =   315
         Left            =   60
         TabIndex        =   48
         Top             =   960
         Width           =   795
      End
      Begin VB.CheckBox chkANIALIForm 
         Caption         =   "Show Incident Details"
         Height          =   255
         Left            =   240
         TabIndex        =   33
         Top             =   600
         Width           =   1935
      End
      Begin VB.CommandButton cmdSelAll 
         Caption         =   "Select All"
         Height          =   315
         Left            =   900
         TabIndex        =   35
         Top             =   960
         Width           =   1215
      End
      Begin VB.CommandButton cmdSendAniAli 
         Caption         =   "Send"
         Height          =   315
         Left            =   2160
         TabIndex        =   34
         Top             =   960
         Width           =   915
      End
      Begin VB.ListBox lstAniAli 
         Height          =   255
         ItemData        =   "fmMain.frx":044E
         Left            =   120
         List            =   "fmMain.frx":0450
         MultiSelect     =   2  'Extended
         Sorted          =   -1  'True
         TabIndex        =   32
         Top             =   300
         Width           =   2955
      End
   End
   Begin VB.Frame frmFilter 
      Caption         =   "frmFilter"
      Height          =   5235
      Left            =   8640
      TabIndex        =   4
      Top             =   360
      Width           =   2895
      Begin VB.ComboBox cmbAgency 
         Height          =   315
         Left            =   60
         TabIndex        =   17
         Text            =   "cmbAgency"
         Top             =   1200
         Width           =   2775
      End
      Begin VB.CheckBox chkJurisdiction 
         Caption         =   "Include All Jurisdictions"
         Height          =   195
         Left            =   60
         TabIndex        =   19
         Top             =   1560
         Width           =   2775
      End
      Begin VB.CheckBox chkDivision 
         Caption         =   "Include All Divisions"
         Height          =   195
         Left            =   60
         TabIndex        =   23
         Top             =   2820
         Width           =   2835
      End
      Begin VB.ListBox lstJurisdiction 
         Height          =   840
         ItemData        =   "fmMain.frx":0452
         Left            =   60
         List            =   "fmMain.frx":0454
         MultiSelect     =   2  'Extended
         TabIndex        =   21
         Top             =   1800
         Width           =   2775
      End
      Begin VB.ListBox lstDivision 
         Height          =   645
         ItemData        =   "fmMain.frx":0456
         Left            =   60
         List            =   "fmMain.frx":0458
         MultiSelect     =   2  'Extended
         TabIndex        =   25
         Top             =   3060
         Width           =   2775
      End
      Begin VB.CommandButton cmdShow 
         Caption         =   "Show Calls"
         Height          =   315
         Left            =   60
         TabIndex        =   27
         Top             =   3780
         Width           =   1335
      End
      Begin VB.TextBox txtFrom 
         Height          =   315
         Left            =   840
         TabIndex        =   13
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox txtTo 
         Height          =   315
         Left            =   1800
         TabIndex        =   15
         Top             =   600
         Width           =   615
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear List"
         Height          =   315
         Left            =   1500
         TabIndex        =   29
         Top             =   3780
         Width           =   1335
      End
      Begin MSComCtl2.DTPicker dtBrowse 
         Height          =   315
         Left            =   1080
         TabIndex        =   11
         Top             =   180
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   81002497
         CurrentDate     =   38020
      End
      Begin VB.Label Label34 
         Caption         =   "Select Agency"
         Height          =   195
         Left            =   60
         TabIndex        =   18
         Top             =   960
         Width           =   1395
      End
      Begin VB.Label lblFrom 
         Alignment       =   1  'Right Justify
         Caption         =   "From:"
         Height          =   255
         Left            =   300
         TabIndex        =   16
         Top             =   660
         Width           =   435
      End
      Begin VB.Label lblTo 
         Alignment       =   1  'Right Justify
         Caption         =   "To:"
         Height          =   255
         Left            =   1440
         TabIndex        =   14
         Top             =   660
         Width           =   255
      End
      Begin VB.Label Label35 
         Caption         =   "Select Date:"
         Height          =   195
         Left            =   60
         TabIndex        =   12
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame frmCommand 
      Caption         =   "frmCommand"
      Height          =   1695
      Left            =   8640
      TabIndex        =   3
      Top             =   6180
      Width           =   2895
      Begin VB.CommandButton cmdExit 
         Caption         =   "Exit"
         Height          =   315
         Left            =   1140
         TabIndex        =   8
         Top             =   1260
         Width           =   1695
      End
      Begin VB.CommandButton cmdRadioMessage 
         Caption         =   "Send Radio Message"
         Height          =   315
         Left            =   1140
         TabIndex        =   7
         Top             =   900
         Width           =   1695
      End
      Begin VB.CommandButton cmdIncident 
         Caption         =   "Send Incident"
         Height          =   315
         Left            =   1140
         TabIndex        =   6
         Top             =   540
         Width           =   1695
      End
      Begin VB.CommandButton cmdANIALI 
         Caption         =   "Send ANIALI"
         Height          =   315
         Left            =   1140
         TabIndex        =   5
         Top             =   180
         Width           =   1695
      End
      Begin VB.Label lblStatus 
         Caption         =   "lblStatus"
         Height          =   255
         Left            =   240
         TabIndex        =   56
         Top             =   480
         Width           =   735
      End
   End
   Begin VB.Frame frmMain 
      Caption         =   "frmMain"
      Height          =   1875
      Left            =   8640
      TabIndex        =   2
      Top             =   60
      Width           =   2895
   End
   Begin MSComctlLib.StatusBar statBar 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   1
      Top             =   7920
      Width           =   11595
      _ExtentX        =   20452
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab 
      Height          =   7815
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   8535
      _ExtentX        =   15055
      _ExtentY        =   13785
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "fmMain.frx":045A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "frmBrowser"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "fmMain.frx":0476
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "frmUnits"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "fmMain.frx":0492
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "frmANIALI"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Tab 3"
      TabPicture(3)   =   "fmMain.frx":04AE
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "frmDebug"
      Tab(3).ControlCount=   1
      Begin VB.Frame frmDebug 
         Caption         =   "frmDebug"
         Height          =   7395
         Left            =   -74940
         TabIndex        =   36
         Top             =   360
         Width           =   8415
         Begin VB.ListBox lstDebug 
            Height          =   7080
            Left            =   60
            TabIndex        =   37
            Top             =   180
            Width           =   8295
         End
      End
      Begin VB.Frame frmANIALI 
         Caption         =   "frmANIALI"
         Height          =   7395
         Left            =   -74940
         TabIndex        =   28
         Top             =   360
         Width           =   8415
         Begin MSComctlLib.ListView lvANIALI 
            Height          =   2235
            Left            =   120
            TabIndex        =   30
            Top             =   240
            Width           =   4215
            _ExtentX        =   7435
            _ExtentY        =   3942
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
      End
      Begin VB.Frame frmUnits 
         Caption         =   "frmUnits"
         Height          =   7395
         Left            =   -74940
         TabIndex        =   20
         Top             =   360
         Width           =   8415
         Begin VB.CheckBox chkActive 
            Caption         =   "Show Active and Waiting Incidents"
            Height          =   255
            Left            =   120
            TabIndex        =   26
            Top             =   240
            Width           =   4095
         End
         Begin MSFlexGridLib.MSFlexGrid flxUnits 
            Height          =   1815
            Left            =   60
            TabIndex        =   24
            Top             =   2400
            Width           =   3975
            _ExtentX        =   7011
            _ExtentY        =   3201
            _Version        =   393216
         End
         Begin MSFlexGridLib.MSFlexGrid flxActive 
            Height          =   1815
            Left            =   60
            TabIndex        =   22
            Top             =   540
            Width           =   3975
            _ExtentX        =   7011
            _ExtentY        =   3201
            _Version        =   393216
         End
      End
      Begin VB.Frame frmBrowser 
         Caption         =   "frmBrowser"
         Height          =   7395
         Left            =   60
         TabIndex        =   9
         Top             =   360
         Width           =   8415
         Begin MSFlexGridLib.MSFlexGrid flxBrowse 
            Height          =   7155
            Left            =   60
            TabIndex        =   10
            Top             =   180
            Width           =   8295
            _ExtentX        =   14631
            _ExtentY        =   12621
            _Version        =   393216
         End
      End
   End
   Begin MSWinsockLib.Winsock tcpSock 
      Left            =   11100
      Top             =   240
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuConnect 
         Caption         =   "&Connect"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuShowDebug 
         Caption         =   "&Show Debug List"
      End
      Begin VB.Menu mnuAbout 
         Caption         =   "&About"
      End
   End
   Begin VB.Menu mnuPopIncidentMenu 
      Caption         =   "PopIncidentMenu"
      Begin VB.Menu mnuSendANIALI 
         Caption         =   "Send ANIALI"
      End
      Begin VB.Menu mnuSendIncident 
         Caption         =   "Send Incident"
      End
   End
   Begin VB.Menu mnuPopUnitMenu 
      Caption         =   "PopUnitMenu"
      Begin VB.Menu mnuRadioMessage 
         Caption         =   "Transmit Radio"
      End
      Begin VB.Menu mnuStopMovement 
         Caption         =   "Stop Movement"
      End
      Begin VB.Menu mnuResumeMovement 
         Caption         =   "Resume Movement"
      End
   End
End
Attribute VB_Name = "fmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Type tIncident
    SourceID As Long
    TimeStart As Variant
    IncId As Long
    MasterNumber As String
    UnitList() As String
    Priority As String
    Problem As String
    Agency As String
    Jurisdiction As String
    Division As String
    CallTaker As String
    PU_Address As String
    PU_Location As String
    PU_City As String
    PU_Lat As Double
    PU_Lon As Double
    DE_Address As String
    DE_Location As String
    DE_City As String
    DE_Code As String
    DE_Lat As Double
    DE_Lon As Double
    NumPats As Integer
    ETimeInQueue As Double
    ETimeChute As Variant
    ETimeOnScene As Variant
    ETimeAtDest As Variant
    ForeColor As Long
    BackColor As Long
    QueueRow As Integer
    Priority_Number As Long
    AgencyID As Long
    Queue As String
End Type

Private Type tVehicle
    UnitName As Variant
    VehID As Long
    AVLID As Long
    MDTID As Long
    RMIID As Long
    AVLEnabled As Boolean
    CurrentLat As Double
    CurrentLon As Double
    CurrentLoc As String
    CurrentCity As String
    SceneLoc As String
    SceneCity As String
    SceneLat As Double
    SceneLon As Double
    DestinationLoc As String
    DestinationCity As String
    DestinationCode As String
    DestinationLat As Double
    DestinationLon As Double
    Status As vcStatus
    QueueRow As Integer
    Station As String
    StationLat As Double
    StationLon As Double
    CurrentDivision As String
    CurrentJurisdiction As String
    ResourceType As String
End Type

Private Type tStatus
    Id As Long
    Description As String
    ForeColor As Long
    BackColor As Long
End Type

Private Type XMLStatus
    UnitID As Long
    FromStat As Integer
    ToStat As Integer
    CallID As Long
    StatTime As Variant
    StationID As Long
End Type
    
Private Type XMLPosition
    UnitID As Long
    CurrLat As Double
    CurrLon As Double
    CurrLoc As String
    CurrCity As String
    LastAVLUpdate As Variant
    Speed As Single
    Heading As Single
    Altitude As Long
End Type

Private Type tPriority
    AgencyID As Long
    PNumber As Long
    ForeColor As Long
    BackColor As Long
End Type

Dim gPriority() As tPriority
Dim gUnitList() As tVehicle
Dim gStatusList() As tStatus
Dim gActiveList() As tIncident

Dim gUnitListLength As Long

Dim gConnectString As String
Dim gPartialMessage As String
Dim gBigUnitListHeight As Long
Dim gSimulationRunning As Boolean
Dim gReceivedWhileProcessing As String
Dim gTCPProcessing As Boolean

Const K_MARGIN = 60

Private Sub chkActive_Click()
    If chkActive.Value = vbChecked Then
        flxUnits.Top = flxActive.Top + flxActive.Height + K_MARGIN
        flxUnits.Height = flxActive.Height
        Call SetUpActiveBrowser
        Call FillActiveIncidentQueue
        Call LoadUnitList
        Call RefreshUnitQueue
    Else
        flxUnits.Top = flxActive.Top
        flxUnits.Height = gBigUnitListHeight
    End If
End Sub

Private Sub chkDivision_Click()
    If chkDivision.Value = vbChecked Then
        lstDivision.Clear
        lstDivision.Enabled = False
    Else
        Call FillDivisionList
        lstDivision.Enabled = True
    End If
End Sub

Private Sub chkJurisdiction_Click()
    If chkJurisdiction.Value = vbChecked Then
        chkDivision.Enabled = False
        chkDivision.Value = vbChecked
        lstJurisdiction.Clear
        lstJurisdiction.Enabled = False
    Else
        Call FillJurisdictionList
        chkDivision.Enabled = True
        chkDivision.Value = vbChecked
        lstJurisdiction.Enabled = True
    End If
End Sub

Private Sub cmbActiveAgency_Click()
    
    chkActiveJurisdiction.Value = vbChecked
    lstActiveJurisdictions.Clear
    lstActiveDivisions.Clear
    
    If cmbActiveAgency.Text <> "<Show All Agencies>" Then
        chkActiveJurisdiction.Enabled = True
    Else
        chkActiveJurisdiction.Enabled = False
    End If

End Sub

Private Sub chkActiveDivision_Click()
    If chkActiveDivision.Value = vbChecked Then
        lstActiveDivisions.Clear
        lstActiveDivisions.Enabled = False
    Else
        Call FillActiveDivisionList
        lstActiveDivisions.Enabled = True
    End If
End Sub

Private Sub chkActiveJurisdiction_Click()
    If chkActiveJurisdiction.Value = vbChecked Then
        chkActiveDivision.Enabled = False
        chkActiveDivision.Value = vbChecked
        lstActiveJurisdictions.Clear
        lstActiveJurisdictions.Enabled = False
    Else
        Call FillActiveJurisdictionList
        chkActiveDivision.Enabled = True
        chkActiveDivision.Value = vbChecked
        lstActiveJurisdictions.Enabled = True
    End If
End Sub

Private Sub cmbAgency_Click()
    
    chkJurisdiction.Value = vbChecked
    lstJurisdiction.Clear
    lstDivision.Clear
    
    If cmbAgency.Text <> "<Show All Agencies>" Then
        chkJurisdiction.Enabled = True
    Else
        chkJurisdiction.Enabled = False
    End If

End Sub

Private Sub cmdClear_Click()
    Call SetUpFilterPane
    Call SetupIncidentBrowser
End Sub

Private Sub cmdClearActiveFilter_Click()
    Call SetUpFilterActivePane
    Call SetUpUnitBrowser
End Sub

Private Sub cmdClearRadio_Click()
    txtTransmit.Text = ""
End Sub

Private Sub cmdExit_Click()
    Call mnuExit_Click
End Sub

Private Sub cmdIncident_Click()
    Call SendIncidentToCAD(flxBrowse.TextMatrix(flxBrowse.Row, K_ACT_ID))
End Sub

Private Sub cmdSendAniAli_Click()
    Dim cMsgNum As String
    Dim cHeader As String
    Dim cMsg As String
    Dim n As Integer
    Dim cUseDialog As String
    
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim cError As String
    
    cUseDialog = IIf(UCase(bsiGetSettings("SystemDefaults", "UseANIALIDialog", "TRUE", gSystemDirectory & "\Simulator.INI")) = "TRUE", "0", "1")
    
    cMsgNum = Format(CN_MSG_CLASS, "0000")
    cHeader = Format(CN_MSG_PASSTHROUGH, "0000") & tcpSock.LocalHostName & "|"
    
    Select Case SSTab.Tab
        Case 0
            '   MsgBox "INCIDENT Tab Selected"
            
            If chkANIALIForm.Value = vbChecked Then
                frmIncident.Caption = "VisiCAD ID " & flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_ID) & " incident details"
                frmIncident.Show
                Call WriteIncidentDetails(flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_ID))
            End If

            For n = 0 To lstAniAli.ListCount - 1
                If lstAniAli.Selected(n) Then
                    cMsg = cHeader & Format(EV_E911, "0000") & "|" & flxBrowse.TextMatrix(flxBrowse.Row, K_FLX_ID) & "," & lstAniAli.List(n) & "," & cUseDialog
                    Call SendTCPMessage(cMsgNum, cMsg)
                    DoEvents
                End If
            Next n
            
            frmANIALIControl.Visible = False
        Case 1
            '   MsgBox "UNITS Tab Selected"
            If chkANIALIForm.Value = vbChecked Then
                frmIncident.Caption = "VisiCAD ID " & flxActive.TextMatrix(flxActive.RowSel, K_ACT_ID) & " incident details"
                frmIncident.Show
                Call WriteIncidentDetails(flxActive.TextMatrix(flxActive.RowSel, K_ACT_ID))
            End If

            For n = 0 To lstAniAli.ListCount - 1
                If lstAniAli.Selected(n) Then
                    cMsg = cHeader & Format(EV_E911, "0000") & "|" & flxActive.TextMatrix(flxActive.Row, K_ACT_ID) & "," & lstAniAli.List(n) & "," & cUseDialog
                    Call SendTCPMessage(cMsgNum, cMsg)
                    DoEvents
                End If
            Next n
            
            frmANIALIControl.Visible = False
        Case 2
            '   MsgBox "ANIALI Tab Selected"
            If chkANIALIForm.Value = vbChecked Then
                '   cSQL = "SELECT Master_Incident_ID FROM Response_ANIALI WHERE ID = " & Right(lvANIALI.SelectedItem.Key, Len(lvANIALI.SelectedItem.Key) - 1)
                '   nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
                '   If nRows > 0 Then
                    frmIncident.Caption = "VisiCAD ANIALI ID " & Right(lvANIALI.SelectedItem.Key, Len(lvANIALI.SelectedItem.Key) - 1) & " incident details"
                    frmIncident.Show
                    Call WriteIncidentDetails(Right(lvANIALI.SelectedItem.Key, Len(lvANIALI.SelectedItem.Key) - 1), True)
                    '   Call WriteIncidentDetails(aSQL(0, 0))
                '   End If
            End If

            For n = 0 To lstAniAli.ListCount - 1
                If lstAniAli.Selected(n) Then
                    cMsg = cHeader & Format(EV_ANIALI, "0000") & "|" & Right(lvANIALI.SelectedItem.Key, Len(lvANIALI.SelectedItem.Key) - 1) & "," & lstAniAli.List(n) & "," & cUseDialog
                    Call SendTCPMessage(cMsgNum, cMsg)
                    DoEvents
                End If
            Next n
    End Select
    
End Sub

Private Sub cmdTransmit_Click()
    Dim UnitRecord As tVehicle
    Dim cMsgNum As String
    Dim cMsg As String
    
    cMsgNum = Format(CN_MSG_CLASS, "0000")
    
    cMsg = Format(CN_MSG_SIM_RADIOMSG, "0000") & tcpSock.LocalHostName & "|" & lblRadioUnit.Caption & "|" & txtTransmit.Text
    
    Call SendTCPMessage(cMsgNum, cMsg)
    
    frmRadio.Visible = False
    
End Sub

Private Sub cmdSelAll_Click()
    Dim n As Integer
    
    For n = 0 To lstAniAli.ListCount - 1
        lstAniAli.Selected(n) = True
    Next n
    
End Sub

Private Sub cmdSetActiveFilter_Click()
    If chkActive.Value = vbChecked Then
        Call SetUpActiveBrowser
        Call FillActiveIncidentQueue
        Call LoadUnitList
        Call RefreshUnitQueue
    Else
        Call LoadUnitList
        Call RefreshUnitQueue
    End If
End Sub

Private Sub cmdShow_Click()
    Dim dFrom As Variant
    Dim dTo As Variant
    
    If bsiValidateTime(txtFrom.Text) Then
        If bsiValidateTime(txtTo.Text) Then
            dFrom = CDate(Format(dtBrowse.Value, "mmm dd yyyy ") & txtFrom.Text)
            dTo = CDate(Format(dtBrowse.Value, "mmm dd yyyy ") & txtTo.Text)
            If dFrom < dTo Then
                '   valid information has been entered. Proceed with the process
                Call SetupIncidentBrowser
                '   Call FillIncidentBrowser(dFrom, dTo, cmbDiv.List(cmbDiv.ListIndex))
                Call FillIncidentBrowser(dFrom, dTo, "")
            Else
                '   from later than to - to os on the next day
                dTo = DateAdd("D", 1, dTo)                  '   change to the next day (same time)
                Call SetupIncidentBrowser
                '   Call FillIncidentBrowser(dFrom, dTo, cmbDiv.List(cmbDiv.ListIndex))
                Call FillIncidentBrowser(dFrom, dTo, "")
            End If
        Else
            '   they didn't fix the to time field, so put them back there
            txtTo.SetFocus
            txtTo.SelStart = 0
            txtTo.SelLength = Len(txtTo.Text)
        End If
    Else
        '   they didn't fix the from time field, so put them back there
        txtFrom.SetFocus
        txtFrom.SelStart = 0
        txtFrom.SelLength = Len(txtFrom.Text)
    End If
    
End Sub

Private Sub cmdWAV_Click()
    Dim cMsgNum As String
    Dim cMsg As String
    Dim cSoundByteDir As String
    
    On Error GoTo ErrorHandler
    
    cSoundByteDir = bsiGetSettings("SystemDefaults", "SoundDirectory", "Q:\TriTech\CADNorth\SimMedia", gSystemDirectory & "\Simulator.INI")

    dlgFile.InitDir = cSoundByteDir
    dlgFile.DialogTitle = "Select Sound Byte File"
    dlgFile.CancelError = True
    dlgFile.Filter = "Sound Files (*.wav)|*.wav"
    dlgFile.ShowOpen
    
    cMsgNum = Format(CN_MSG_CLASS, "0000")
    cMsg = Format(CN_MSG_SIM_SOUNDBYTE, "0000")
    
    cMsg = cMsg & tcpSock.LocalHostName & "|" & lblRadioUnit.Caption & "|" & dlgFile.FileName
        
    Call SendTCPMessage(cMsgNum, cMsg)
    
    frmRadio.Visible = False
            
    Exit Sub
    
ErrorHandler:
    If Err.Number = cdlCancel Then
        Exit Sub
    Else
        Call AddToDebugList("[cmdWAV_Click] Error occurred in the file selection routine:" & Str(Err.Number) & " " & Err.Description)
    End If
End Sub

Private Sub flxActive_RowColChange()
    '   Call AddToDebugList("[flxActive_SelChange]")
End Sub

Private Sub flxActive_SelChange()
    '   Call AddToDebugList("[flxActive_SelChange]")
End Sub

Private Sub Form_Load()
    Dim frmLeft As Long
    Dim frmTop As Long
    Dim frmHeight As Long
    Dim frmWidth As Long

    frmSplash.LoadStatus = "Loading Setup Values"
    
    Me.Caption = App.Title & " v" & App.Major & "." & App.Minor & " build " & App.Revision
    
    Call SetUpFrames
    
    mnuPopUnitMenu.Visible = False
    mnuPopIncidentMenu.Visible = False
    mnuSendIncident.Enabled = False
        
    cmdTransmit.Enabled = False
    cmdANIALI.Enabled = False
    cmdIncident.Enabled = False
    
    gConnectString = FixConnectString
    
    tmTimer.Interval = 500
    tmTimer.Enabled = False
    
    Call SetupPriorities
    Call SetupIncidentBrowser
    Call SetUpFilterPaneComponents
    Call SetUpFilterPane
    Call SetUpFilterActivePane
    Call SetUpFilterActivePaneComponents
    Call SetUpCommandPane
    
    frmSplash.LoadStatus = "Configuring Browsers"
    
    Call SetUpStatusBar
    Call SetUpDebugTab
    Call SetUpUnitsTab
    Call SetUpANIALITab
    Call SetUpANIALIFrame
    Call SetUpRadioFrame
    Call InitStatus
    
    frmSplash.LoadStatus = "Loading Lists"
    
    Call LoadUnitList
    
    frmSplash.LoadStatus = "Connecting to Master Console"
    
    Call mnuConnect_Click
    
    Call SetEnableOnAllControls(False)
    
    Call RequestSimulationStatus
    
    SSTab.TabVisible(3) = False
    SSTab.Tab = 0
    frmBrowser.ZOrder
    
    Me.Show
    
    Call SSTab_Click(0)
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Select Case UnloadMode
        Case vbFormControlMenu      '   The user chose the Close command from the Control menu on the form.
        Case vbFormCode             '   The Unload statement is invoked from code.
        Case vbAppWindows           '   The current Microsoft Windows operating environment session is ending.
        Case vbAppTaskManager       '   The Microsoft Windows Task Manager is closing the application.
        Case vbFormMDIForm          '   An MDI child form is closing because the MDI form is closing.
        Case vbFormOwner            '   A form is closing because its owner is closing.
    End Select
    
    If MsgBox("Do you want to shut down " & App.Title & "?", vbYesNo + vbQuestion, "Application Exit") = vbYes Then
        If mnuConnect.Checked Then
            If DisconnectIPCServer(tcpSock, lstDebug) Then
                Unload frmAbout
                Unload frmSplash
                Unload Me           '   shut down application
            Else
                Cancel = 1          '   stop exit
            End If
        Else
            Unload frmAbout
            Unload frmSplash
            Unload Me           '   shut down application
        End If
    Else
        Cancel = 1          '   stop exit
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    '   End
End Sub

Private Sub lstActiveJurisdictions_Click()
    If chkActiveDivision.Value = vbUnchecked Then
        Call chkActiveDivision_Click
    End If
End Sub

Private Sub lstJurisdiction_Click()
    If chkDivision.Value = vbUnchecked Then
        Call chkDivision_Click
    End If
End Sub

Private Sub lvANIALI_BeforeLabelEdit(Cancel As Integer)
    Cancel = 1
End Sub

Private Sub lvANIALI_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Dim PrevKey As Integer
    
    PrevKey = lvANIALI.SortKey
    
    lvANIALI.SortKey = ColumnHeader.Index - 1
    
    If lvANIALI.SortKey <> PrevKey Then
        If lvANIALI.SortKey = 0 Then
            lvANIALI.SortOrder = lvwDescending
        Else
            lvANIALI.SortOrder = lvwAscending
        End If
    Else
        If lvANIALI.SortOrder <> lvwAscending Then
            lvANIALI.SortOrder = lvwAscending
        Else
            lvANIALI.SortOrder = lvwDescending
        End If
    End If

End Sub

Private Sub mnuConnect_Click()
    Dim bUseXML As Boolean
    
    bUseXML = IIf(UCase(bsiGetSettings("SystemDefaults", "UseXMLPort", "1", gSystemDirectory & "\Simulator.INI")) = "1", True, False)

    If mnuConnect.Checked Then
        If DisconnectIPCServer(tcpSock, lstDebug) Then
            mnuConnect.Checked = False
            tmTimer.Enabled = False
            statBar.Panels.Item("Status").Text = "Disconnected"
            statBar.Panels.Item("Message").Text = ""
        Else
            statBar.Panels.Item("Message").Text = "Error disconnecting from IPC Server - Check Debug Log"
        End If
    Else
        If ConnectToIPCServer(tcpSock, lstDebug, , , , bUseXML) Then
            mnuConnect.Checked = True
            tmTimer.Enabled = True
            statBar.Panels.Item("Status").Text = "Connected"
            statBar.Panels.Item("Message").Text = ""
        Else
            statBar.Panels.Item("Message").Text = "Unable to connect to IPC Server - Check Debug Log"
        End If
    End If
End Sub

Private Sub mnuRadioMessage_Click()
    frmRadio.Visible = True
    frmRadio.ZOrder
    txtTransmit.SetFocus
End Sub

Private Sub mnuSendANIALI_Click()
    frmANIALIControl.Visible = True
    frmANIALIControl.ZOrder
    lstAniAli.SetFocus
End Sub

Private Sub mnuSendIncident_Click()
    Select Case SSTab.Tab
        Case 0  '   Incidents
            Call SendIncidentToCAD(flxBrowse.TextMatrix(flxBrowse.Row, K_FLX_ID))
        Case 1  '   Units
            Call SendIncidentToCAD(flxActive.TextMatrix(flxActive.Row, K_ACT_ID))
        Case 2  '   ANIALI
        Case 3  '   Debug
    End Select
End Sub

Private Sub mnuShowDebug_Click()
    If mnuShowDebug.Checked Then
        mnuShowDebug.Checked = False
        SSTab.TabVisible(3) = False
    Else
        mnuShowDebug.Checked = True
        SSTab.TabVisible(3) = True
    End If
End Sub

Private Sub mnuStopMovement_Click()
    '   insert code here to cause the unit to stop moving in the system
End Sub

Private Sub mnuResumeMovement_Click()
    '   insert code here to recalculate the route to the unit's destination and resume moving immediately
End Sub

Private Sub SSTab_Click(PreviousTab As Integer)
    Select Case SSTab.Tab
        Case 0  '   Incidents
            frmMain.Visible = False
            frmFilter.Visible = True
            frmFilterActive.Visible = False
            frmANIALIControl.Visible = False
            frmRadio.Visible = False
            dtBrowse.SetFocus
        Case 1  '   Units
            frmMain.Visible = False
            frmFilter.Visible = False
            frmFilterActive.Visible = True
            frmANIALIControl.Visible = False
            frmRadio.Visible = False
            cmbActiveAgency.SetFocus
        Case 2  '   ANIALI
            frmMain.Visible = False
            frmFilter.Visible = False
            frmFilterActive.Visible = False
            frmANIALIControl.Visible = True
            frmRadio.Visible = False
            lstAniAli.SetFocus
        Case 3  '   Debug
            frmMain.Visible = False
            frmFilter.Visible = False
            frmFilterActive.Visible = False
            frmANIALIControl.Visible = False
            frmRadio.Visible = False
    End Select
End Sub

Private Sub tmTimer_Timer()
    If gSimulationRunning Then
        statBar.Panels.Item("Time").Text = Format(Now, "HH:NN:SS")
    End If
End Sub

Private Sub txtFrom_GotFocus()
    txtFrom.SelStart = 0
    txtFrom.SelLength = Len(txtFrom.Text)
End Sub

Private Sub txtFrom_LostFocus()
    Dim TimeString As String
        
    If txtFrom.Text <> "" Then
        TimeString = txtFrom.Text
        
        If bsiValidateTime(TimeString) Then
            txtFrom.Text = TimeString
        End If
    End If
End Sub

Private Sub txtTo_GotFocus()
    txtTo.SelStart = 0
    txtTo.SelLength = Len(txtTo.Text)
End Sub

Private Sub txtTo_LostFocus()
    Dim TimeString As String
    
    If txtTo.Text <> "" Then
        TimeString = txtTo.Text
        
        If bsiValidateTime(TimeString) Then
            txtTo.Text = TimeString
        End If
    End If
End Sub

Private Sub mnuAbout_Click()
    Dim cLicense As String
    Dim cAppName As String
    Dim cLicenseBrief As String
    
    cAppName = App.Title & " v" & App.Major & "." & App.Minor & " build " & App.Revision
    
    cLicenseBrief = cLicenseBrief & "LICENCE INFORMATION" & vbCrLf & vbCrLf
    cLicenseBrief = cLicenseBrief & "    COMPANY : " & KeyInfo.ClientName & vbCrLf
    cLicenseBrief = cLicenseBrief & "              " & KeyInfo.ClientCity & vbCrLf
    cLicenseBrief = cLicenseBrief & "              " & KeyInfo.ClientProv & vbCrLf
    
    cLicense = cLicense & "LICENCE INFORMATION" & vbCrLf & vbCrLf
    cLicense = cLicense & "    COMPANY : " & KeyInfo.ClientName & vbCrLf
    cLicense = cLicense & "              " & KeyInfo.ClientCity & vbCrLf
    cLicense = cLicense & "              " & KeyInfo.ClientProv & vbCrLf
    cLicense = cLicense & "              " & KeyInfo.ClientSite & vbCrLf
    cLicense = cLicense & "    EXPIRES : " & IIf(Trim(KeyInfo.AppExpires) = "", "<never>", KeyInfo.AppExpires) & vbCrLf

    cLicense = cLicense & "       MODE : " & KeyInfo.AppMode & vbCrLf
    cLicense = cLicense & "     SERIAL : " & KeyInfo.AppSerial & vbCrLf

    Call frmAbout.CNShow(Me, 10, cLicenseBrief, cLicense, cAppName)
    
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub SetUpFrames()

    SSTab.Top = fmMain.ScaleTop
    SSTab.Height = fmMain.ScaleHeight - statBar.Height
    SSTab.Left = fmMain.ScaleLeft
    
    SSTab.TabsPerRow = 4
    SSTab.Tabs = 4
    
    SSTab.TabCaption(0) = "Incidents"
    SSTab.TabCaption(1) = "Units"
    SSTab.TabCaption(2) = "ANIALI"
    SSTab.TabCaption(3) = "Debug"
    
    frmBrowser.Caption = "History Browser"
    frmBrowser.Top = SSTab.TabHeight + K_MARGIN / 2
    frmBrowser.Height = SSTab.Height - SSTab.TabHeight - K_MARGIN
    frmBrowser.Left = K_MARGIN
    
    frmUnits.Caption = "Current System Activity"
    frmUnits.Top = frmBrowser.Top
    frmUnits.Height = frmBrowser.Height
    frmUnits.Width = frmBrowser.Width
    frmUnits.Left = frmBrowser.Left
    frmUnits.ZOrder
    
    frmANIALI.Caption = "Browsing ANIALI List"
    frmANIALI.Top = frmBrowser.Top
    frmANIALI.Left = frmBrowser.Left
    frmANIALI.Height = frmBrowser.Height
    frmANIALI.Width = frmBrowser.Width
    frmUnits.ZOrder
    
    frmDebug.Caption = "Debugging Info"
    frmDebug.Top = frmBrowser.Top
    frmDebug.Left = frmBrowser.Left
    frmDebug.Height = frmBrowser.Height
    frmDebug.Width = frmBrowser.Width
    frmDebug.ZOrder
    
    frmMain.Caption = ""
    frmMain.Top = fmMain.ScaleTop
    frmMain.Width = fmMain.ScaleWidth - SSTab.Width - K_MARGIN
    frmMain.Left = fmMain.ScaleWidth - frmMain.Width
    frmMain.Height = SSTab.Height - frmCommand.Height - K_MARGIN
    
    frmCommand.Caption = ""
    frmCommand.Left = frmMain.Left
    frmCommand.Width = frmMain.Width
    
    frmFilter.Caption = "Set Filters"
    frmFilter.Top = frmMain.Top
    frmFilter.Left = frmMain.Left
    frmFilter.Height = frmMain.Height
    frmFilter.Width = frmMain.Width
    
    frmFilterActive.Caption = "Set Filters"
    frmFilterActive.Top = frmMain.Top
    frmFilterActive.Left = frmMain.Left
    frmFilterActive.Height = frmMain.Height
    frmFilterActive.Width = frmMain.Width

    frmANIALIControl.Caption = "Workstations"
    frmANIALIControl.Top = frmMain.Top
    frmANIALIControl.Height = frmMain.Height
    frmANIALIControl.Left = frmMain.Left
    frmANIALIControl.Width = frmMain.Width
    
    With frmRadio
        .Caption = "Radio Dialog"
        .Top = frmMain.Top
        .Height = frmMain.Height
        .Left = frmMain.Left
        .Width = frmMain.Width
        .Visible = False
    End With
    
End Sub

Private Sub SetupIncidentBrowser()

    On Error GoTo ERH
    
    With flxBrowse
        .Height = frmBrowser.Height - 4 * K_MARGIN
        .FixedCols = 0
        .FixedRows = 1
        .SelectionMode = flexSelectionByRow
        .FocusRect = flexFocusNone
        .Cols = 15
        .Rows = 40
        .Row = 0
        .ColAlignment(K_FLX_ID) = flexAlignRightCenter
        .ColAlignment(K_FLX_Address) = flexAlignLeftCenter
        .ColAlignment(K_FLX_City) = flexAlignLeftCenter
    End With
    
    flxBrowse.Clear
    flxBrowse.SelectionMode = flexSelectionByRow

    flxBrowse.FixedCols = 0
    flxBrowse.Cols = 20
    
    flxBrowse.TextMatrix(0, K_FLX_ROLL) = "[+]"
    flxBrowse.TextMatrix(0, K_FLX_ID) = "ID"
    flxBrowse.TextMatrix(0, K_FLX_DateTime) = "Date/Time"
    flxBrowse.TextMatrix(0, K_FLX_Address) = "Address"
    flxBrowse.TextMatrix(0, K_FLX_City) = "City"
    flxBrowse.TextMatrix(0, K_FLX_Quad) = "Div"
    flxBrowse.TextMatrix(0, K_FLX_Problem) = "Problem"
    flxBrowse.TextMatrix(0, K_FLX_Unit) = "Unit"
    flxBrowse.TextMatrix(0, K_FLX_UnitType) = "Unit Type"
    flxBrowse.TextMatrix(0, K_FLX_CallTaker) = "Call Taker"
    flxBrowse.TextMatrix(0, K_FLX_Destination) = "Destination"
    flxBrowse.TextMatrix(0, K_FLX_Assigned) = "Assigned"
    flxBrowse.TextMatrix(0, K_FLX_Enroute) = "Enroute"
    flxBrowse.TextMatrix(0, K_FLX_AtScene) = "AtScene"
    flxBrowse.TextMatrix(0, K_FLX_Transport) = "Transport"
    flxBrowse.TextMatrix(0, K_FLX_AtDest) = "AtDest"
    flxBrowse.TextMatrix(0, K_FLX_AvailAtDest) = "AvailAtDest"
    flxBrowse.TextMatrix(0, K_FLX_Available) = "Available"
    flxBrowse.TextMatrix(0, K_FLX_Latitude) = "Latitude"
    flxBrowse.TextMatrix(0, K_FLX_Longitude) = "Longitude"
    
    flxBrowse.ColAlignment(K_FLX_ROLL) = flexAlignCenterCenter

    flxBrowse.ColWidth(K_FLX_ROLL) = 350                             '   [+] header
    flxBrowse.ColWidth(K_FLX_DateTime) = 2 * flxBrowse.ColWidth(19)      '   Date/Time
    flxBrowse.ColWidth(K_FLX_Address) = 2 * flxBrowse.ColWidth(19)      '   Address
    flxBrowse.ColWidth(K_FLX_Quad) = 0.5 * flxBrowse.ColWidth(19)    '   Quadrant
    flxBrowse.ColWidth(K_FLX_Problem) = 2 * flxBrowse.ColWidth(19)      '   Problem
    flxBrowse.ColWidth(K_FLX_CallTaker) = 1.5 * flxBrowse.ColWidth(19)    '   Call Taker
    flxBrowse.ColWidth(K_FLX_Destination) = 2 * flxBrowse.ColWidth(19)     '   Destination
    
    flxBrowse.Row = 0
    
    '    If mapInc.ActiveMap.DataSets.Item(1).Name = "My Pushpins" Then
    '        mapInc.ActiveMap.DataSets.Item(1).Delete
    '    End If
    
    Exit Sub

ERH:

    '   MsgBox "Err:" & Str(Err.Number) & "  Description: " & Err.Description, vbInformation + vbOKOnly, "SetupIncidentBrowser"

End Sub

Private Sub SetUpActiveBrowser(Optional ByVal nRows As Long = -1)
    
    If nRows = -1 Then
        nRows = 40
    Else
        nRows = nRows + 1
    End If
    
    With flxActive
        .Cols = 17
        .Rows = nRows
        .FixedCols = 0
        .FixedRows = 1
        .SelectionMode = flexSelectionByRow
        '   .HighLight = flexHighlightWithFocus
        .FocusRect = flexFocusNone
        .Row = 0
        .ColAlignment(K_ACT_ID) = flexAlignRightCenter
        .ColAlignment(K_ACT_Address) = flexAlignLeftCenter
        .ColAlignment(K_ACT_City) = flexAlignLeftCenter
        .ColAlignment(K_ACT_QUEUE) = flexAlignCenterCenter
    
        .Clear
        .SelectionMode = flexSelectionByRow

        .TextMatrix(0, K_ACT_ID) = "ID"
        .TextMatrix(0, K_ACT_QUEUE) = "Q"
        .TextMatrix(0, K_ACT_DateTime) = "Date/Time"
        .TextMatrix(0, K_ACT_Address) = "Address"
        .TextMatrix(0, K_ACT_Location) = "Location"
        .TextMatrix(0, K_ACT_City) = "City"
        .TextMatrix(0, K_ACT_Problem) = "Problem"
        .TextMatrix(0, K_ACT_Priority) = "Priority"
        .TextMatrix(0, K_ACT_CallTaker) = "Call Taker"
        .TextMatrix(0, K_ACT_Agency) = "Agency"
        .TextMatrix(0, K_ACT_Jurisdiction) = "Jurisdiction"
        .TextMatrix(0, K_ACT_Division) = "Division"
        .TextMatrix(0, K_ACT_Latitude) = "Latitude"
        .TextMatrix(0, K_ACT_Longitude) = "Longitude"
        .TextMatrix(0, K_ACT_PriorityNumber) = "PriorityNumber"
        .TextMatrix(0, K_ACT_MasterNumber) = "MasterNumber"
        .TextMatrix(0, K_ACT_AgencyID) = "AgencyID"
    
        .ColWidth(K_ACT_ID) = 800
        .ColWidth(K_ACT_QUEUE) = 300
        .ColWidth(K_ACT_DateTime) = 2 * flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_Address) = 2 * flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_Location) = 2 * flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_City) = flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_Agency) = flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_Jurisdiction) = flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_Division) = flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_Problem) = 2 * flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_Priority) = 2 * flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_CallTaker) = 1.5 * flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_Latitude) = flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_Longitude) = flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_PriorityNumber) = flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_MasterNumber) = flxActive.ColWidth(K_ACT_ID)
        .ColWidth(K_ACT_AgencyID) = flxActive.ColWidth(K_ACT_ID)
    
        .Col = 0
        .Row = 1
    End With

End Sub

Private Sub SetUpUnitBrowser()

    With flxUnits
        .Clear
        .Rows = 50
        .Cols = 11
        .FixedCols = 0
        .FixedRows = 1
        .SelectionMode = flexSelectionByRow
        .FocusRect = flexFocusNone
        .ColWidth(0) = 750
        .ColWidth(1) = .ColWidth(0) * 3
        .ColWidth(3) = .ColWidth(0) * 3
        .ColWidth(5) = .ColWidth(0) * 2
        .ColWidth(6) = .ColWidth(0) * 2
        .ColAlignment(K_UNIT_UNIT) = flexAlignLeftCenter
        .ColAlignment(K_UNIT_CURRLOC) = flexAlignLeftCenter
        .ColAlignment(K_UNIT_CURRCITY) = flexAlignLeftCenter
        .ColAlignment(K_UNIT_DESTLOC) = flexAlignLeftCenter
        .ColAlignment(K_UNIT_DESTCITY) = flexAlignLeftCenter
        .ColAlignment(K_UNIT_JUR) = flexAlignLeftCenter
        .ColAlignment(K_UNIT_DIV) = flexAlignLeftCenter
        .ColAlignment(K_UNIT_CURRLAT) = flexAlignRightCenter
        .ColAlignment(K_UNIT_CURRLON) = flexAlignRightCenter
        .ColAlignment(K_UNIT_DESTLAT) = flexAlignRightCenter
        .ColAlignment(K_UNIT_DESTLON) = flexAlignRightCenter
        .TextMatrix(0, K_UNIT_UNIT) = "Unit"
        .TextMatrix(0, K_UNIT_CURRLOC) = "CurrLoc"
        .TextMatrix(0, K_UNIT_CURRCITY) = "CurrCity"
        .TextMatrix(0, K_UNIT_DESTLOC) = "DestLoc"
        .TextMatrix(0, K_UNIT_DESTCITY) = "DestCity"
        .TextMatrix(0, K_UNIT_JUR) = "Jurisdiction"
        .TextMatrix(0, K_UNIT_DIV) = "Division"
        .TextMatrix(0, K_UNIT_CURRLAT) = "CurrLat"
        .TextMatrix(0, K_UNIT_CURRLON) = "CurrLon"
        .TextMatrix(0, K_UNIT_DESTLAT) = "DestLat"
        .TextMatrix(0, K_UNIT_DESTLON) = "DestLon"
        .Row = 0
        .Col = 1
        .CellAlignment = flexAlignLeftCenter   '   correct header alignment
        .Col = 2
        .CellAlignment = flexAlignLeftCenter   '   correct header alignment
        .Col = 5
        .CellAlignment = flexAlignLeftCenter   '   correct header alignment
        .Col = 6
        .CellAlignment = flexAlignLeftCenter   '   correct header alignment
    End With
    
End Sub

Private Sub SetUpFilterPane()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    Dim LastAgency As String
    
    On Error GoTo SetUpFilterPane_ERH
    
    dtBrowse.Value = Now
    txtFrom.Text = "00:00"
    txtTo.Text = "23:59"
    
    cSQL = "SELECT Agency_Type FROM AgencyTypes"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
        cmbAgency.Clear
        cmbAgency.AddItem "<Show All Agencies>"
        For n = 0 To nRows - 1
            cmbAgency.AddItem aSQL(0, n)
        Next n
        LastAgency = bsiGetSettings("SystemDefaults", "LastAgency", "***NOTDEFINED***", gSystemDirectory & "\Simulator.INI")
        cmbAgency.Text = IIf(LastAgency = "***NOTDEFINED***", cmbAgency.List(0), LastAgency)
    Else
        Call MsgBox("No Agencies returned. Check ODBC Settings.", vbCritical + vbOKOnly, "Loading Agencies")
        cmbAgency.Clear
        cmbAgency.AddItem "<LOAD ERROR>"
    End If
    
    chkJurisdiction.Value = vbChecked
    chkJurisdiction.Enabled = False
    
    Exit Sub
    
SetUpFilterPane_ERH:
    AddToDebugList ("[ERROR] in SetUpFilterPane Loading Agency Filter (" & Err.Number & ") - " & Err.Description & " - " & cError)
End Sub

Private Sub SetUpCommandPane()
    Dim PaneHeight As Long
    
    cmdExit.Width = frmCommand.Width - 2 * K_MARGIN
    cmdExit.Top = frmCommand.Height - cmdExit.Height - K_MARGIN
    cmdExit.Left = K_MARGIN
    
    cmdANIALI.Visible = False
    cmdIncident.Visible = False
    cmdRadioMessage.Visible = False
    
    PaneHeight = frmCommand.Height - cmdExit.Height - K_MARGIN
    
    lblStatus.Font = "Arial"
    lblStatus.FontSize = 24
    lblStatus.FontBold = True
    lblStatus.ForeColor = RGB(0, 0, 0)
    lblStatus.Caption = "Waiting ..."
    lblStatus.Alignment = vbCenter
    lblStatus.Width = frmCommand.Width - 2 * K_MARGIN
    lblStatus.Left = K_MARGIN
    lblStatus.Height = 550
    lblStatus.Top = PaneHeight / 2 - lblStatus.Height / 2
    
End Sub

Private Sub SetUpFilterPaneComponents()
    Dim PaneHeight As Long
    
    cmdClear.Width = frmFilter.Width / 2 - 1.5 * K_MARGIN
    cmdShow.Width = cmdClear.Width
    
    cmdClear.Top = frmFilter.Height - cmdClear.Height - K_MARGIN
    cmdClear.Left = frmFilter.Width - cmdClear.Width - K_MARGIN
    
    cmdShow.Top = cmdClear.Top
    cmdShow.Left = cmdClear.Left - cmdShow.Width - K_MARGIN
    
    PaneHeight = cmdClear.Top - (cmbAgency.Top + cmbAgency.Height)
    
    lstJurisdiction.Width = frmFilter.Width - 2 * K_MARGIN
    lstDivision.Width = lstJurisdiction.Width
    cmbAgency.Width = lstJurisdiction.Width
    
    chkJurisdiction.Top = cmbAgency.Top + cmbAgency.Height + K_MARGIN
    lstJurisdiction.Top = chkJurisdiction.Top + chkJurisdiction.Height + K_MARGIN
    lstJurisdiction.Height = PaneHeight / 2 - 2 * chkJurisdiction.Height - 2 * K_MARGIN
    
    chkDivision.Top = lstJurisdiction.Top + lstJurisdiction.Height + K_MARGIN
    lstDivision.Top = chkDivision.Top + chkDivision.Height + K_MARGIN
    lstDivision.Height = cmdClear.Top - lstDivision.Top - K_MARGIN
    
End Sub

Private Sub SetUpFilterActivePane()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    Dim LastActiveAgency As String
    
    On Error GoTo SetUpFilterActivePane_ERH
    
    cSQL = "SELECT Agency_Type FROM AgencyTypes"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
        cmbActiveAgency.Clear
        cmbActiveAgency.AddItem "<Show All Agencies>"
        For n = 0 To nRows - 1
            cmbActiveAgency.AddItem aSQL(0, n)
        Next n
        LastActiveAgency = bsiGetSettings("SystemDefaults", "LastActiveAgency", "***NOTDEFINED***", gSystemDirectory & "\Simulator.INI")
        cmbActiveAgency.Text = "<Show All Agencies>"
    Else
        Call MsgBox("No Agencies returned. Check ODBC Settings.", vbCritical + vbOKOnly, "Loading Agencies")
        cmbActiveAgency.Clear
        cmbActiveAgency.AddItem "<LOAD ERROR>"
    End If
    
    chkActiveJurisdiction.Value = vbChecked
    chkActiveJurisdiction.Enabled = False
    
    Exit Sub
    
SetUpFilterActivePane_ERH:
    AddToDebugList ("[ERROR] in SetUpFilterActivePane Loading Agency Filter (" & Err.Number & ") - " & Err.Description & " - " & cError)
End Sub

Private Sub SetUpFilterActivePaneComponents()
    Dim PaneHeight As Long
    
    cmdClearActiveFilter.Width = frmFilterActive.Width / 2 - 1.5 * K_MARGIN
    cmdSetActiveFilter.Width = cmdClearActiveFilter.Width
    
    cmdClearActiveFilter.Top = frmFilterActive.Height - cmdClearActiveFilter.Height - K_MARGIN
    cmdClearActiveFilter.Left = frmFilterActive.Width - cmdClearActiveFilter.Width - K_MARGIN
    
    cmdSetActiveFilter.Top = cmdClearActiveFilter.Top
    cmdSetActiveFilter.Left = cmdClearActiveFilter.Left - cmdSetActiveFilter.Width - K_MARGIN
    
    PaneHeight = cmdClearActiveFilter.Top - (cmbActiveAgency.Top + cmbActiveAgency.Height)
    
    lstActiveJurisdictions.Width = frmFilterActive.Width - 2 * K_MARGIN
    lstActiveDivisions.Width = lstActiveJurisdictions.Width
    cmbActiveAgency.Width = lstActiveJurisdictions.Width
    
    chkActiveJurisdiction.Top = cmbAgency.Top + cmbAgency.Height + K_MARGIN
    lstActiveJurisdictions.Top = chkActiveJurisdiction.Top + chkActiveJurisdiction.Height + K_MARGIN
    lstActiveJurisdictions.Height = PaneHeight / 2 - 2 * chkActiveJurisdiction.Height - 2 * K_MARGIN
    
    chkActiveDivision.Top = lstActiveJurisdictions.Top + lstActiveJurisdictions.Height + K_MARGIN
    lstActiveDivisions.Top = chkActiveDivision.Top + chkActiveDivision.Height + K_MARGIN
    lstActiveDivisions.Height = frmFilterActive.Height - lstActiveDivisions.Top - 4 * K_MARGIN
    
End Sub

Private Sub SetUpStatusBar()
    With statBar
        .Panels.Clear
        .Panels.Add , "Time"
        .Panels.Item("Time").Width = 1400
        .Panels.Item("Time").Alignment = sbrCenter
        .Panels.Add , "Status"
        .Panels.Item("Status").Width = 2200
        .Panels.Item("Status").Alignment = sbrCenter
        .Panels.Add , "Message"
        .Panels.Item("Message").Width = .Width - .Panels.Item("Time").Width - .Panels.Item("Status").Width
        .Panels.Item("Message").Alignment = sbrLeft
    End With
End Sub

Private Sub SetUpDebugTab()
    lstDebug.Height = frmDebug.Height - 3 * K_MARGIN
End Sub

Private Sub SetUpUnitsTab()
    flxActive.Top = chkActive.Top + chkActive.Height + K_MARGIN
    flxActive.Left = K_MARGIN
    flxActive.Width = frmUnits.Width - 2 * K_MARGIN
    flxActive.Height = (frmUnits.Height - chkActive.Top - chkActive.Height) / 2 - 2 * K_MARGIN
    
    flxUnits.Top = flxActive.Top
    flxUnits.Left = flxActive.Left
    flxUnits.Width = flxActive.Width
    flxUnits.Height = frmUnits.Height - chkActive.Top - chkActive.Height - 3 * K_MARGIN
    flxUnits.ZOrder
    
    Call SetUpActiveBrowser
    
    Call SetUpUnitBrowser
    
    gBigUnitListHeight = flxUnits.Height
    
End Sub

Private Sub SetUpANIALITab()

    With lvANIALI
        .View = lvwReport
        .Top = K_MARGIN * 3
        .Left = K_MARGIN
        .Height = frmANIALI.Height - 4 * K_MARGIN
        .Width = frmANIALI.Width - 2 * K_MARGIN
        .Font = "Arial"
        .FullRowSelect = True
        .HideSelection = False
        .ColumnHeaders.Add 1, , "TimeReceived", 1575
        .ColumnHeaders.Add 2, , "Phone", 1305
        .ColumnHeaders.Add 3, , "Address", 2100
        .ColumnHeaders.Add 4, , "City", 1250
        .ColumnHeaders.Add 5, , "State", 555
        .ColumnHeaders.Add 6, , "CallClass", 1000
        .ColumnHeaders.Add 7, , "Customer", 2050
        .ColumnHeaders.Add 8, , "Latitude", 1000
        .ColumnHeaders.Add 9, , "Longitude", 1100
        .ColumnHeaders.Add 10, , "OriginalTextMessage", 20000
        .GridLines = True
        .Sorted = True
        .SortKey = 1                '   Sort Key column
        .SortOrder = lvwAscending
    End With
    
    Call FillANIALIList
    
    frmANIALI.Visible = True
    frmANIALI.ZOrder

End Sub

Private Sub SetUpANIALIFrame()
    cmdSendAniAli.Width = (frmANIALIControl.Width - (3 * K_MARGIN)) / 2
    cmdSendAniAli.Top = frmANIALIControl.Height - cmdSendAniAli.Height - K_MARGIN
    cmdSendAniAli.Left = frmANIALIControl.Width - cmdSendAniAli.Width - K_MARGIN
    cmdSelAll.Width = (frmANIALIControl.Width - (3 * K_MARGIN)) / 2
    cmdSelAll.Top = cmdSendAniAli.Top
    cmdSelAll.Left = cmdSendAniAli.Left - cmdSelAll.Width - K_MARGIN
    cmdSendCellular.Visible = False
'    cmdSendCellular.Width = cmdSendAniAli.Width
'    cmdSendCellular.Top = cmdSendAniAli.Top
'    cmdSendCellular.Left = cmdSelAll.Left - cmdSendCellular.Width - K_MARGIN
    chkANIALIForm.Top = cmdSendAniAli.Top - chkANIALIForm.Height - (2 * K_MARGIN)
    chkANIALIForm.Left = (frmANIALIControl.Width / 2) - (chkANIALIForm.Width / 2)
    lstAniAli.Top = 4 * K_MARGIN
    lstAniAli.Left = K_MARGIN
    lstAniAli.Height = chkANIALIForm.Top - lstAniAli.Top - K_MARGIN
    lstAniAli.Width = frmANIALIControl.Width - 2 * K_MARGIN
    
    Call SetupWorkstations
    
    Call FillANIALIList
    
End Sub

Private Sub SetUpRadioFrame()
'    frmRadio.Top = frmMain.Top
'    frmRadio.Height = frmMain.Height
'    frmRadio.Left = frmMain.Left
'    frmRadio.Width = frmMain.Width
    cmdTransmit.Width = (frmRadio.Width - (4 * K_MARGIN)) / 3
    cmdClearRadio.Width = cmdTransmit.Width
    cmdWAV.Width = cmdTransmit.Width
    cmdTransmit.Top = frmRadio.Height - cmdTransmit.Height - K_MARGIN
    cmdTransmit.Left = frmRadio.Width - cmdTransmit.Width - K_MARGIN
    cmdClearRadio.Top = cmdTransmit.Top
    cmdClearRadio.Left = cmdTransmit.Left - cmdTransmit.Width - K_MARGIN
    cmdWAV.Top = cmdTransmit.Top
    cmdWAV.Left = cmdClearRadio.Left - cmdWAV.Width - K_MARGIN
    txtTransmit.Top = 4 * K_MARGIN + lblRadioUnit.Height
    txtTransmit.Left = K_MARGIN
    txtTransmit.Height = cmdClearRadio.Top - txtTransmit.Top - K_MARGIN
    txtTransmit.Width = frmRadio.Width - 2 * K_MARGIN
    lblRadioUnit.Caption = ""
    txtTransmit.Text = ""

    frmRadio.Visible = False
    
End Sub

Private Sub SetupWorkstations()
    Dim WSList() As String
    Dim WSString As String
    Dim n As Integer
    
    lstAniAli.Clear
    
    WSString = bsiGetSettings("Workstations", "WSList", "", gSystemDirectory & "\Simulator.INI")
    
    WSList = Split(WSString, "|")
    
    For n = 0 To UBound(WSList)
        lstAniAli.AddItem WSList(n)
    Next n
    
End Sub

Private Sub FillDivisionList()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    Dim cJurisdictions As String
    
    On Error GoTo FillDivisionList_ERH
    
    For n = 0 To lstJurisdiction.ListCount - 1
        If lstJurisdiction.Selected(n) Then
            cJurisdictions = cJurisdictions & "'" & lstJurisdiction.List(n) & "',"
        End If
    Next n
    
    cJurisdictions = Left(cJurisdictions, Len(cJurisdictions) - 1)
    
    cSQL = "SELECT d.DivName FROM Division d WHERE d.JurisdictionID IN (Select ID FROM Jurisdiction WHERE Name IN (" & cJurisdictions & "))"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
        lstDivision.Clear
        For n = 0 To nRows - 1
            lstDivision.AddItem aSQL(0, n)
        Next n
    Else
        Call MsgBox("No Divisions returned. Check ODBC Settings." & vbCrLf & "Error: " & cError, vbCritical + vbOKOnly, "Loading Divisions")
        lstDivision.Clear
        lstDivision.AddItem "<LOAD ERROR>"
    End If
    
    Exit Sub
    
FillDivisionList_ERH:
    AddToDebugList ("[ERROR] in FillDivisionList Loading Division List (" & Err.Number & ") - " & Err.Description & " - " & cError)
End Sub

Private Sub FillJurisdictionList()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    Dim LastAgency As String
    
    On Error GoTo FillJurisdictionList_ERH
    
    cSQL = "SELECT j.Name FROM Jurisdiction j JOIN AgencyTypes a ON j.AgencyID = a.ID WHERE a.Agency_Type = '" & cmbAgency.Text & "'"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
        lstJurisdiction.Clear
        For n = 0 To nRows - 1
            lstJurisdiction.AddItem aSQL(0, n)
        Next n
    Else
        Call MsgBox("No Jurisdictions returned for Agency '" & cmbAgency.Text & "'. Check ODBC Settings." & vbCrLf & "Error: " & cError, vbCritical + vbOKOnly, "Loading Jurisdictions")
        lstJurisdiction.Clear
        lstJurisdiction.AddItem "<LOAD ERROR>"
    End If
    
    Exit Sub

FillJurisdictionList_ERH:
    AddToDebugList ("[ERROR] in FillJurisdictionList Loading Jurisdiction List (" & Err.Number & ") - " & Err.Description & " - " & cError)
End Sub

Private Sub InitStatus()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRecs As Long
    Dim n As Long
    
    On Error GoTo ERH
    
'    Private Type Status
'        ID as Long
'        Description As String
'        ForeColor As Long
'        BackColor As Long
'    End Type

    cSQL = "Select ID, Description, ForeColor, BackColor " _
                    & " from Status " _
                    & " Order by ID "

    nRecs = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    AddToDebugList "[Loading] " & Str(nRecs) & " Status Records Found"
    
    If nRecs > 0 Then
        ReDim gStatusList(0 To nRecs - 1)
        For n = 0 To nRecs - 1
            gStatusList(n).Id = aSQL(0, n)
            If n <> aSQL(0, n) Then
                AddToDebugList "[OOPS!] Status ID not Sequential n=" & Str(n) & " Status ID=" & aSQL(0, n)
            End If
            gStatusList(n).Description = aSQL(1, n)
            gStatusList(n).ForeColor = IIf(aSQL(2, n) > 0, aSQL(2, n), 1)       '   eliminate non-black blacks
            gStatusList(n).BackColor = IIf(aSQL(3, n) > 0, aSQL(3, n), 1)       '   eliminate non-black blacks
        Next n
    End If
    
    Exit Sub
    
ERH:
    
    AddToDebugList ("[ERROR in InitStatus] No.:" & Str(Err.Number) & " " & Err.Description)

End Sub

Private Sub FillActiveDivisionList()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    Dim cJurisdictions As String
    
    On Error GoTo FillActiveDivisionList_ERH
    
    For n = 0 To lstActiveJurisdictions.ListCount - 1
        If lstActiveJurisdictions.Selected(n) Then
            cJurisdictions = cJurisdictions & "'" & lstActiveJurisdictions.List(n) & "',"
        End If
    Next n
    
    If cJurisdictions <> "" Then
    
        cJurisdictions = Left(cJurisdictions, Len(cJurisdictions) - 1)
        
        cSQL = "SELECT d.DivName FROM Division d WHERE d.JurisdictionID IN (Select ID FROM Jurisdiction WHERE Name IN (" & cJurisdictions & "))"
        
        nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
        
        If nRows > 0 Then
            lstActiveDivisions.Clear
            For n = 0 To nRows - 1
                lstActiveDivisions.AddItem aSQL(0, n)
            Next n
        Else
            Call MsgBox("No Divisions returned. Check ODBC Settings." & vbCrLf & "Error: " & cError, vbCritical + vbOKOnly, "Loading Divisions")
            lstActiveDivisions.Clear
            lstActiveDivisions.AddItem "<LOAD ERROR>"
        End If
    
    End If
    
    Exit Sub
    
FillActiveDivisionList_ERH:
    AddToDebugList ("[ERROR] in FillActiveDivisionList Loading Division List (" & Err.Number & ") - " & Err.Description & " - " & cError)
End Sub

Private Sub FillActiveJurisdictionList()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    Dim LastAgency As String
    
    On Error GoTo FillActiveJurisdictionList_ERH
    
    cSQL = "SELECT j.Name FROM Jurisdiction j JOIN AgencyTypes a ON j.AgencyID = a.ID WHERE a.Agency_Type = '" & cmbActiveAgency.Text & "'"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
        lstActiveJurisdictions.Clear
        For n = 0 To nRows - 1
            lstActiveJurisdictions.AddItem aSQL(0, n)
        Next n
    Else
        Call MsgBox("No Jurisdictions returned for Agency '" & cmbAgency.Text & "'. Check ODBC Settings." & vbCrLf & "Error: " & cError, vbCritical + vbOKOnly, "Loading Jurisdictions")
        lstActiveJurisdictions.Clear
        lstActiveJurisdictions.AddItem "<LOAD ERROR>"
    End If
    
    Exit Sub

FillActiveJurisdictionList_ERH:
    AddToDebugList ("[ERROR] in FillActiveJurisdictionList Loading Jurisdiction List (" & Err.Number & ") - " & Err.Description & " - " & cError)
End Sub

Private Sub FillANIALIList()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    Dim iItem As ListItem
    
    On Error GoTo FillANIALIList_ERH

    '    .ColumnHeaders.Add 1, , "TimeReceived", 1200
    '    .ColumnHeaders.Add 2, , "Phone", 800
    '    .ColumnHeaders.Add 3, , "Address", 1800
    '    .ColumnHeaders.Add 4, , "City", 800
    '    .ColumnHeaders.Add 5, , "State", 800
    '    .ColumnHeaders.Add 6, , "CallClass", 1000
    '    .ColumnHeaders.Add 7, , "Customer", 1000
    '    .ColumnHeaders.Add 8, , "Latitude", 1000
    '    .ColumnHeaders.Add 9, , "Longitude", 1000
    '    .ColumnHeaders.Add 10, , "OriginalTextMessage", 20000
    
    cSQL = "SELECT TOP " & bsiGetSettings("SIMOP", "ANIALIROWCOUNT", "500", gSystemDirectory & "\Simulator.INI") & " "
    cSQL = cSQL & " TimeCallReceived, PhoneNumber, Address, City, State, ClassOfService, CustomerName, " _
                & " CAST(Latitude as Float) / 1000000, CAST(Longitude as Float) / -1000000, " _
                & " CONVERT(CHAR(1000), OriginalTextMessage), ID " _
                & " FROM Response_ANIALI " _
                & " ORDER BY TimeCallReceived DESC "
        
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
        lvANIALI.ListItems.Clear
        For n = 0 To nRows - 1
            Set iItem = lvANIALI.ListItems.Add(, "K" & Format(aSQL(10, n)), IIf(IsNull(aSQL(0, n)), "", Format(aSQL(0, n), "YYYY.MM.DD HH:NN:SS")))
            iItem.SubItems(1) = IIf(IsNull(aSQL(1, n)), "", aSQL(1, n))
            iItem.SubItems(2) = IIf(IsNull(aSQL(2, n)), "", aSQL(2, n))
            iItem.SubItems(3) = IIf(IsNull(aSQL(3, n)), "", aSQL(3, n))
            iItem.SubItems(4) = IIf(IsNull(aSQL(4, n)), "", aSQL(4, n))
            iItem.SubItems(5) = IIf(IsNull(aSQL(5, n)), "", aSQL(5, n))
            iItem.SubItems(6) = IIf(IsNull(aSQL(6, n)), "", aSQL(6, n))
            iItem.SubItems(7) = IIf(IsNull(aSQL(7, n)), "", aSQL(7, n))
            iItem.SubItems(8) = IIf(IsNull(aSQL(8, n)), "", aSQL(8, n))
            iItem.SubItems(9) = IIf(IsNull(aSQL(9, n)), "", aSQL(9, n))
        Next n
    End If
    
    Exit Sub
    
FillANIALIList_ERH:
    AddToDebugList ("[ERROR] in FillANIALIList (" & Err.Number & ") - " & Err.Description & " - " & cError)
End Sub

Private Sub FillIncidentBrowser(dFrom As Variant, dTo As Variant, sDivision As String)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long, M As Long
    Dim PrioColor As tPriority
    Dim PrevID As Long
    Dim MaxLat As Double
    Dim MaxLon As Double
    Dim MinLat As Double
    Dim MinLon As Double
    Dim Lat As Double
    Dim Lon As Double
    Dim Progress As String
    Dim cJFilter As String
    Dim cDFilter As String
    
    Dim cError As String
    
    On Error GoTo FillIncidentBrowser_ERH
    
    Progress = "Start of FillIncidentBrowser"

    cSQL = "select rm.ID, " _
            & " rm.Time_CallEnteredQueue, " _
            & " rm.address, " _
            & " rm.city, " _
            & " rm.division, " _
            & " rm.problem, " _
            & " rv.radio_name, " _
            & " re.description, " _
            & " rm.CallTaking_Performed_By, " _
            & " rt.Location_name, " _
            & " rv.time_assigned, " _
            & " rv.time_enroute, " _
            & " rv.time_ArrivedAtScene, " _
            & " rt.time_Depart_Scene, " _
            & " rt.time_arrive_destination, " _
            & " rv.time_delayed_availability, " _
            & " rv.time_call_cleared, " _
            & " rm.latitude, " _
            & " rm.longitude, "
    cSQL = cSQL & " rv.ID, " _
            & " rt.ID, " _
            & " rm.Priority_Number, " _
            & " a.ID, " _
            & " rm.master_incident_number "
    cSQL = cSQL & "from Response_master_incident rm left outer join " _
            & "Response_Vehicles_assigned rv on rv.master_incident_id = rm.id left outer join " _
            & "Response_Transports rt on rt.vehicle_assigned_id = rv.id left outer join " _
            & "Vehicle ve on ve.ID = rv.Vehicle_ID left outer join " _
            & "Resource re on ve.PrimaryResource_ID = re.ID join " _
            & "AgencyTypes a on rm.Agency_Type = a.Agency_Type " _
            & "Where rm.Time_CallEnteredQueue between '" & Format(dFrom, "mmm dd yyyy hh:nn:ss") & "' and '" & Format(dTo, "mmm dd yyyy hh:nn:ss") & "' " _
            & " and rm.Priority_Number <> 0 " _
            & " and rm.ID is not NULL "

    If cmbAgency.Text <> "<Show All Agencies>" Then
        cSQL = cSQL & " and rm.Agency_Type ='" & Trim(cmbAgency.Text) & "' "
    End If
    
    If chkJurisdiction.Value = vbUnchecked Then
        cJFilter = GetJurisdictionList()
        If cJFilter <> "" Then
            cSQL = cSQL & " and rm.Jurisdiction in (" & cJFilter & ") "
        End If
    End If
    
    If chkDivision.Value = vbUnchecked Then
        cDFilter = GetDivisionList()
        If cDFilter <> "" Then
            cSQL = cSQL & " and rm.Division in (" & cDFilter & ") "
        End If
    End If

'    If sDivision <> "All" Then
'        cSQL = cSQL & " and rm.Division ='" & Trim(sDivision) & "' "
'    End If
    
    cSQL = cSQL & "order by rm.Time_CallEnteredQueue "
    
    ' txtEnterField.Text = cSQL
    
    Progress = "Executing Query"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
   
    ' MsgBox "Query executed: " & Str(NumRows) & " rows returned"
    
    If nRows > 0 Then
        
        Progress = "Evaluating Min/Max"

        flxBrowse.Rows = nRows + 1
        flxBrowse.ScrollBars = flexScrollBarBoth
        
        PrevID = -1
        
        '    If IsNumeric(aSQL(17, n)) And IsNumeric(aSQL(18, n)) Then
        '
        '        MaxLat = (Val(aSQL(17, 0)) / 1000000) - gLatitudeAdjust
        '        MaxLon = (Val(aSQL(18, 0)) / -1000000) - gLongitudeAdjust
        '        MinLat = (Val(aSQL(17, 0)) / 1000000) - gLatitudeAdjust
        '        MinLon = (Val(aSQL(18, 0)) / -1000000) - gLongitudeAdjust
        '
        '    End If
        
        For n = 0 To nRows - 1
        
            Progress = " >> Fill Browser Row Loop for n =" & Str(n) & ", Master Incident Number = " & IIf(IsNull(aSQL(23, n)), "NULL", aSQL(23, n))
        
            '    If IsNumeric(aSQL(17, n)) And IsNumeric(aSQL(18, n)) Then
            '
            '        Lat = (Val(IIf(IsNull(aSQL(17, n)), 0#, aSQL(17, n)) / 1000000)) - gLatitudeAdjust
            '        Lon = (Val(IIf(IsNull(aSQL(18, n)), 0#, aSQL(18, n)) / -1000000)) - gLongitudeAdjust
            '
            '    Else
            '
            '        Lat = 0 - gLatitudeAdjust
            '        Lon = 0 - gLongitudeAdjust
            '
            '    End If
            
            PrioColor = bsiGetPriority(IIf(IsNull(aSQL(21, n)), 0, aSQL(21, n)), IIf(IsNull(aSQL(22, n)), 0, aSQL(22, n)))
            
            '    aMapLocs(n).IncId = aSQL(0, n)
            '    aMapLocs(n).Address = IIf(IsNull(aSQL(2, n)), "NULL", aSQL(2, n))
            '    aMapLocs(n).Problem = IIf(IsNull(aSQL(5, n)), "NULL", aSQL(5, n))
            '    aMapLocs(n).Lat = Lat
            '    aMapLocs(n).Lon = Lon

            If PrevID <> aSQL(0, n) Then
                PrevID = aSQL(0, n)
                flxBrowse.Row = n + 1
                flxBrowse.Col = K_FLX_ID
                flxBrowse.CellForeColor = PrioColor.ForeColor
                flxBrowse.CellBackColor = PrioColor.BackColor
                flxBrowse.Text = aSQL(0, n)
                
                '    If Not (Lat = 0 Or Lon = 0) Then
                '        Set IncLoc = mapInc.ActiveMap.GetLocation(Lat, Lon)
                '        Set IncPin = mapInc.ActiveMap.AddPushpin(IncLoc, aSQL(0, n))
                '        IncPin.Symbol = 41
                '        IncPin.Note = aSQL(2, n) & Chr(13) & aSQL(5, n)
                '        If Lat > MaxLat Then MaxLat = Lat           '   get the box coords
                '        If Lon > MaxLon Then MaxLon = Lon
                '        If Lat < MinLat Then MinLat = Lat
                '        If Lon < MinLon Then MinLon = Lon
                '    End If
                
            Else
            '    flxBrowse.Row = n
            '    flxBrowse.Col = K_FLX_ROLL
            '    flxBrowse.Text = "[+]"
                flxBrowse.Row = n + 1
                flxBrowse.Col = K_FLX_ID
                flxBrowse.Text = " "
            End If
            
            For M = K_FLX_DateTime To flxBrowse.Cols - 1
            
                Progress = " >> FillIncidentBrowser - Filling Time Fields - m =" & Str(M)

                flxBrowse.Row = n + 1
                flxBrowse.Col = M
                flxBrowse.CellForeColor = PrioColor.ForeColor
                flxBrowse.CellBackColor = PrioColor.BackColor
                Select Case M
                    Case K_FLX_Address
                        flxBrowse.CellAlignment = flexAlignLeftCenter
                    Case K_FLX_Problem
                        flxBrowse.CellAlignment = flexAlignLeftCenter
                    Case K_FLX_Unit
                        flxBrowse.CellAlignment = flexAlignLeftCenter
                End Select
                If M > K_FLX_UnitType And M < K_FLX_Latitude Then
                    flxBrowse.Text = Format(aSQL(M - 1, n), "HH:NN:SS")     '   time fields
                ElseIf M = K_FLX_Latitude Then
                    Progress = Progress & ", Lat=" & Lat
                    flxBrowse.Text = Format(Lat, "###0.000000")             '   latitude ... using the applied correction
                ElseIf M = K_FLX_Longitude Then
                    Progress = Progress & ", Lon=" & Lon
                    flxBrowse.Text = Format(Lon, "###0.000000")             '   longitude ... using the applied correction
                ElseIf M = K_FLX_DateTime Then
                    flxBrowse.Text = Format(aSQL(M - 1, n), "MMM DD YYYY HH:NN:SS")   '   Incident Time
                Else
                    flxBrowse.Text = IIf(IsNull(aSQL(M - 1, n)), " ", aSQL(M - 1, n))   '   text fields
                End If
                
            Next M
            
        Next n
        
        flxBrowse.Row = 0
        
    Else
    
        Debug.Print cError
        
    End If

   On Error GoTo 0
   Exit Sub

FillIncidentBrowser_ERH:

    On Error Resume Next
    
    Call AddToDebugList("[ERROR] in procedure FillIncidentBrowser of Form frmMain:" & Err.Number & ", " & Err.Description & Progress)
    Call AddToDebugList("        nRows     = " & Str(nRows))
    Call AddToDebugList("        n         = " & Str(n))
    Call AddToDebugList("        rm.ID     = " & IIf(IsNull(aSQL(0, n)), "NULL", aSQL(0, n)))
    Call AddToDebugList("        Address   = " & IIf(IsNull(aSQL(2, n)), "NULL", aSQL(2, n)))
    Call AddToDebugList("        Problem   = " & IIf(IsNull(aSQL(5, n)), "NULL", aSQL(5, n)))
    Call AddToDebugList("        Priority  = " & IIf(IsNull(aSQL(21, n)), "NULL", aSQL(21, n)))
    Call AddToDebugList("        AgencyID  = " & IIf(IsNull(aSQL(22, n)), "NULL", aSQL(22, n)))
    Call AddToDebugList("        Latitude  = " & IIf(IsNull(aSQL(17, n)), "NULL", aSQL(17, n)))
    Call AddToDebugList("        Longitude = " & IIf(IsNull(aSQL(18, n)), "NULL", aSQL(18, n)))
    Call AddToDebugList("    Raw Latitude  = >" & aSQL(17, n) & "<")
    Call AddToDebugList("    Raw Longitude = >" & aSQL(18, n) & "<")
    Call AddToDebugList("        Latitude  = " & IIf(IsEmpty(aSQL(17, n)), "EMPTY", aSQL(17, n)))
    Call AddToDebugList("        Longitude = " & IIf(IsEmpty(aSQL(18, n)), "EMPTY", aSQL(18, n)))
    Call AddToDebugList("        Latitude  = " & IIf(IsMissing(aSQL(17, n)), "MISSING", aSQL(17, n)))
    Call AddToDebugList("        Longitude = " & IIf(IsMissing(aSQL(18, n)), "MISSING", aSQL(18, n)))
    Call AddToDebugList("        Latitude  = " & IIf(Not IsNumeric(aSQL(17, n)), "NOT NUMERIC", aSQL(17, n)))
    Call AddToDebugList("        Longitude = " & IIf(Not IsNumeric(aSQL(18, n)), "NOT NUMERIC", aSQL(18, n)))
    Call AddToDebugList("        rm.M.I.N. = " & IIf(IsNull(aSQL(23, n)), "NULL", aSQL(23, n)))
    Call AddToDebugList("        ForeColor = " & Str(PrioColor.ForeColor))
    Call AddToDebugList("        BackColor = " & Str(PrioColor.BackColor))

    On Error GoTo 0
    
End Sub

Private Sub flxBrowse_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim n As Integer
    Dim SelectedInc As String
    
    On Error GoTo ERH
    
    Do While flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_ID) = " "
        flxBrowse.Row = flxBrowse.Row - 1
        flxBrowse.RowSel = flxBrowse.Row
        flxBrowse.Col = 0
        flxBrowse.ColSel = flxBrowse.Cols - 1
    Loop
    
    cmdTransmit.Enabled = False
    
    SelectedInc = flxBrowse.TextMatrix(flxBrowse.Row, K_FLX_ID)
    
    If SelectedInc <> "" Then
        '   cmdIncident.Enabled = True
        cmdANIALI.Enabled = True
    Else
        cmdIncident.Enabled = False
        cmdANIALI.Enabled = False
    End If

    Select Case Button
        Case vbLeftButton
            
        Case vbRightButton
            flxBrowse.Row = flxBrowse.MouseRow
            flxBrowse.Col = 0
            flxBrowse.RowSel = flxBrowse.Row
            flxBrowse.ColSel = flxBrowse.Cols - 1
            
            SelectedInc = flxBrowse.TextMatrix(flxBrowse.Row, K_FLX_ID)
            
            If SelectedInc <> "" Then
                If gSimulationRunning Then
                    mnuSendIncident.Enabled = True
                    cmdANIALI.Enabled = True
                    mnuSendANIALI.Enabled = True
                Else
                    mnuSendIncident.Enabled = False
                    cmdANIALI.Enabled = False
                    mnuSendANIALI.Enabled = False
                End If
                
                PopupMenu mnuPopIncidentMenu
                
            End If
    
    End Select
    
    Exit Sub
    
ERH:
    Resume Next
End Sub

Private Sub flxUnits_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim n As Integer
    Dim SelectedUnit As String
    
    On Error GoTo ERH
    
    cmdANIALI.Enabled = False
    cmdIncident.Enabled = False
    
    Select Case Button
        Case vbLeftButton
            
        Case vbRightButton
            flxUnits.Row = flxUnits.MouseRow
            flxUnits.Col = 0
            flxUnits.RowSel = flxUnits.Row
            flxUnits.ColSel = flxUnits.Cols - 1
            
            SelectedUnit = flxUnits.TextMatrix(flxUnits.Row, K_UNIT_UNIT)
            
            If SelectedUnit <> "" Then
                lblRadioUnit.Caption = SelectedUnit
        
                If gSimulationRunning Then
                    mnuSendIncident.Enabled = True
                    cmdTransmit.Enabled = True
                    mnuRadioMessage.Enabled = True
                    mnuStopMovement.Enabled = True
                    mnuResumeMovement.Enabled = True
                Else
                    mnuSendIncident.Enabled = False
                    cmdTransmit.Enabled = False
                    mnuRadioMessage.Enabled = False
                    mnuStopMovement.Enabled = False
                    mnuResumeMovement.Enabled = False
                End If
                    
                PopupMenu mnuPopUnitMenu
            Else
                cmdTransmit.Enabled = False
            End If
    
    End Select
    
    Exit Sub
    
ERH:
    Resume Next
End Sub

Private Sub flxActive_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim SelectedInc As String
    Dim nIncID As Long
    
    On Error GoTo ERH

    cmdTransmit.Enabled = False
    
    Select Case Button
        Case vbLeftButton
        
            frmFilterActive.Visible = True
            frmFilterActive.ZOrder
            
            If flxActive.Row - 1 <= UBound(gActiveList, 1) Then
                nIncID = gActiveList(flxActive.Row - 1).IncId
                Call LoadUnitList(nIncID)
            Else
                Call LoadUnitList
            End If
            
            Call RefreshUnitQueue
            
        Case vbRightButton
            flxActive.Row = flxActive.MouseRow
            flxActive.Col = 0
            flxActive.RowSel = flxActive.Row
            flxActive.ColSel = flxActive.Cols - 1
            
            SelectedInc = flxActive.TextMatrix(flxActive.Row, K_FLX_ID)
            
            If SelectedInc <> "" Then
                If gSimulationRunning Then
                    mnuSendIncident.Enabled = True
                    cmdANIALI.Enabled = True
                    mnuSendANIALI.Enabled = True
                Else
                    mnuSendIncident.Enabled = False
                    cmdANIALI.Enabled = False
                    mnuSendANIALI.Enabled = False
                End If
                
                PopupMenu mnuPopIncidentMenu
                
            Else
                cmdIncident.Enabled = False
                cmdANIALI.Enabled = False
            End If
    
    End Select
    
    Exit Sub
    
ERH:
    Resume Next
End Sub

Private Sub SetupPriorities()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim n As Integer
    
    On Error GoTo ERH
    
    cSQL = "select AgencyTypeID, Priority, Forecolor, Backcolor from Priority"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    If nRows > 0 Then
        ReDim gPriority(0 To nRows - 1)
        
        For n = 0 To nRows - 1
            gPriority(n).AgencyID = aSQL(0, n)
            gPriority(n).PNumber = aSQL(1, n)
            If IsNull(aSQL(2, n)) Or aSQL(2, n) = 0 Then
                gPriority(n).ForeColor = 1
            Else
                gPriority(n).ForeColor = aSQL(2, n)
            End If
            If IsNull(aSQL(3, n)) Then
                gPriority(n).BackColor = 1
            Else
                gPriority(n).BackColor = IIf(aSQL(3, n) = 0, 1, aSQL(3, n))
            End If
        Next n
        
    Else
        AddToDebugList "Error: Retrieved " & Str(nRows) & " rows from Priority"
    End If
    
    Exit Sub
    
ERH:
    AddToDebugList "Error(LoadPriorities): " & Str(Err.Number) & "  " & Err.Description
End Sub

Private Function bsiGetPriority(ByVal nPriority As Long, ByVal AgencyID As Long) As tPriority
    Dim n As Integer
    
    On Error GoTo ERH
    
    bsiGetPriority.BackColor = 1
    bsiGetPriority.ForeColor = 1
    
    For n = 0 To UBound(gPriority)
        If gPriority(n).PNumber = nPriority And gPriority(n).AgencyID = AgencyID Then
            bsiGetPriority = gPriority(n)
            Exit For
        End If
    Next n
    
    Exit Function

ERH:
    AddToDebugList "Error(GetPriority): " & Str(Err.Number) & "  " & Err.Description
End Function

Private Function FixConnectString() As String
    Dim Temp As Variant
    Dim sTemp As String
    Dim n As Integer
    
    sTemp = bsiGetSettings("SystemPreferences", "ODBCPassword", "UID=tritech;PWD=europa;DSN=system;", gSystemDirectory & "\System.INI")
    Temp = Split(sTemp, ";")
    sTemp = ""
    
    For n = 0 To UBound(Temp)
        If Temp(n) <> "" And Temp(n) <> "ODBC" Then
            sTemp = sTemp & Temp(n) & ";"
        End If
    Next n
        
    FixConnectString = sTemp
    
End Function

Private Sub tcpSock_Close()
    AddToDebugList "[Event] Socket Closed"
End Sub

Private Sub tcpSock_Connect()
    AddToDebugList "[Event] Connected"
End Sub

Private Sub tcpSock_ConnectionRequest(ByVal requestID As Long)
    AddToDebugList "[Event] Connection Request Received"
End Sub

Private Sub tcpSock_DataArrival(ByVal bytesTotal As Long)
    Dim sTemp As String
    Dim aMessage As Variant
    Dim msgLen As Long                      '   changed from Integer - very large messages would cause an overflow error
    Dim sReceived As String
    Dim Buffer As String
    Dim cASCII As String
    Dim n As Integer, i As Integer
    
'    AddToDebugList "[Event] DataArrival" & Str(bytesTotal)

    tcpSock.GetData sReceived           '   grab the new data as soon as we get here
    
    gPartialMessage = gPartialMessage & sReceived       '   and save it where it won't get whomped by the next message
        
    If Not gTCPProcessing Then
    
        Buffer = gPartialMessage
        gPartialMessage = ""
        
        aMessage = Split(Buffer, Chr(vcIP_EOM))      '   split buffer into separate messages at the termination character ASCII 3
        
        Do While UBound(aMessage, 1) > 0
        
            For n = 0 To UBound(aMessage) - 1
                msgLen = Len(aMessage(n))
                If msgLen > 3 Then
                    aMessage(n) = Right(aMessage(n), msgLen - 1)    '   strip off the leading chr(vcIP_BOM)
                    Call ProcessMessage(aMessage(n))
                Else
                    aMessage(n) = Right(aMessage(n), msgLen - 1)
                    cASCII = ""
                    For i = 1 To Len(aMessage(n))
                        cASCII = cASCII & "[" & Trim(Str(Asc(Mid(aMessage(n), i, 1)))) & "]"
                    Next i
                    AddToDebugList "[ODD THING] >" & aMessage(n) & "< ASCII:" & cASCII
                End If
                
                DoEvents
                
            Next n
            
               '   aMessage(n) will either be empty or contain a partial message because we split the buffer at the chr(3) msg terminator
        
            Buffer = aMessage(n) & gPartialMessage
            
            gPartialMessage = ""
            
            aMessage = Split(Buffer, Chr(vcIP_EOM))       '   split buffer into separate messages at the termination character ASCII 3
        
        Loop
        
        If UBound(aMessage, 1) >= 0 Then
            gPartialMessage = aMessage(UBound(aMessage, 1))
        Else
            gPartialMessage = ""
        End If
        
        gTCPProcessing = False
    
    Else
    
        Call AddToDebugList("[DEBUG] in Sub tcpSock_DataArrival - Data received while processing - " & Len(gPartialMessage) & " bytes.")
    
    End If
    
    Exit Sub
    
tcpSock_DataArrivalERH:
    Call AddToDebugList("[ERROR] in Sub tcpSock_DataArrival - " & Err.Description & " (" & Err.Number & ")")
    gTCPProcessing = False
End Sub

Private Sub tcpSock_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    
    AddToDebugList "[Event IPC] Error" & Str(Number) & ": " & Description
    AddToDebugList "[Info]  Source: " & Source & " Scode:" & Str(Scode)
    AddToDebugList "[Info]  HelpFile: " & HelpFile & " HelpContext:" & Str(HelpContext)
    AddToDebugList "[Info]  CancelDisplay: " & Switch(CancelDisplay, "TRUE", Not CancelDisplay, "FALSE")
    AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock)

End Sub

Private Sub ProcessMessage(ByVal cMessage As String)
    Dim aMsg() As String
    Dim nNum As Integer
    Dim cMsg As String
    Dim cXML As String
    
    On Error GoTo ProcessMessage_ERH
    
    aMsg = Split(cMessage, Chr(vcIP_XML))       '   in case we have an XML payload, split it into legacy and XML
    
    nNum = Val(Left(aMsg(0), 4))
    cMsg = Right(aMsg(0), Len(aMsg(0)) - 4)
    
    If UBound(aMsg, 1) > 0 Then cXML = aMsg(1)
    
    If mnuShowDebug.Checked Then AddToDebugList (cMessage)
    
    Select Case nNum
        Case CN_MSG_CLASS
            Call ProcessCNMessage(cMsg)
        Case Else
            Call ProcessCADMessage(nNum, cMsg)
    End Select
    Exit Sub
    
ProcessMessage_ERH:
    Call AddToDebugList("[ERROR] in Function ProcessMessage - " & Err.Description & " (" & Err.Number & ") - Message: >" & Message & "<")
End Sub

Private Sub ProcessCNMessage(cMessage As String)
    Dim nNum As Integer
    Dim cMsg As String
    
    On Error GoTo ProcessCNMessage_ERH
    
    cNum = Left(cMessage, 4)
    cMsg = Right(cMessage, Len(cMessage) - 4)
    
    Select Case cNum
        Case CN_MSG_SIM_STATUS
            Call AddToDebugList("Simulation Status Query from " & cMsg)
            '   Call PauseSimulation(cMsg)
        Case CN_MSG_PAUSED_SIMULATION
            Call AddToDebugList("Simulation Paused by " & cMsg)
            Call PausedSimulation(cMsg)
        Case CN_MSG_RUNNING_SIMULATION
            Call AddToDebugList("Simulation Running on " & cMsg)
            Call ResumeSimulation(cMsg)
        Case CN_MSG_SIM_MASTER_SHUTDOWN
            Call AddToDebugList(cMsg)
            Call MasterConsoleShutdown(cMsg)
        '    Case CN_MSG_PASSTHROUGH
        '        Call AddToDebugList("Simulation passthrough message:" & cMsg)
        '        '   Call ProcessCNMessage(cMsg)
        Case Else
    End Select

    Exit Sub
    
ProcessCNMessage_ERH:
    Call AddToDebugList("[ERROR] in Function ProcessCNMessage - " & Err.Description & " (" & Err.Number & ") - Message: >" & Message & "<")
End Sub

Private Sub ProcessCADMessage(nMsgNum As Integer, cMsgBody As String)
    Dim StatusXML As XMLStatus
    Dim PositionXML As XMLPosition
    Dim aArray As Variant
    Dim n As Integer
    
    On Error GoTo ProcessCADMessage_ERH
    
    Select Case nMsgNum
        Case NP_READ_VEHICLE
            aArray = Split(cMsgBody, ",")           '   sometimes the NP_READ_VEHICLE message has a string of comma-separated vehicle IDs
            For n = 0 To UBound(aArray)             '   and we need to process all of them
                If Val(aArray(n)) > 0 Then          '   trap funky message with empty element in the list - 2014.07.29 BM
                    Call RefreshUnitInfo(Val(aArray(n)))
                End If
            Next n
        Case NP_NEW_RESPONSE
            Call FillActiveIncidentQueue
        Case NP_REMOVE_RESPONSE
            Call FillActiveIncidentQueue(Val(cMsgBody))
        Case NP_READ_RESPONSE_MASTER_INCIDENT
            Call FillActiveIncidentQueue(Val(cMsgBody))
        Case NP_READ_RESPONSE_TRANSPORT
            '   Call UpdateActiveIncident(cMsgBody)
        Case NP_READ_RESPONSE_DESTINATION
            '   Call UpdateActiveIncident(cMsgBody)
        Case NP_READ_MULTI_ASSIGN_VEHICLE
            '   Call UnitCancelled(msgBody)
        Case NP_CHANGED_RESPONSE_DESTINATION
            '   Call UpdateActiveIncident(cMsgBody)
        Case NP_READ_VEHICLE_STATUS
            StatusXML = ParseXMLStatus(cMsgBody)
            Call RefreshUnitInfo(StatusXML.UnitID)
            If StatusXML.ToStat = vcStatus.Notified Then
                Call FillActiveIncidentQueue                    '   change in incident status implied by change to Notified
            End If
        Case NP_READ_VEHICLE_POSITION
            PositionXML = ParseXMLPosition(cMsgBody)
            Call RefreshUnitInfo(PositionXML.UnitID)
    End Select
        
    Exit Sub
    
ProcessCADMessage_ERH:
    Call AddToDebugList("[ERROR] in Function ProcessCADMessage - " & Err.Description & " (" & Err.Number & ") - Message: >" & Message & "<")
End Sub

Private Function ParseXMLStatus(Message As String) As XMLStatus
    Dim result As XMLStatus
    Dim nStart As Integer
    Dim nStop As Integer
    
    '00:36:41 [0244][214][NP_READ_VEHICLE_STATUS                  ]
    '<Vehicle ID="166"  ReadVehicleSent= "-1">
    '    <FromStatus>13</FromStatus>
    '    <ToStatus>14</ToStatus>
    '    <Master_Incident_ID>2291</Master_Incident_ID>
    '    <TimeStamp>Apr 17 2003 00:36:38</TimeStamp>
    '    <StationID>34</StationID>
    '</Vehicle>
    
    'Beginning with v5.6, this is the new structure of the message      2014.07.17 BM
    '0244
    '<?xml version="1.0" encoding="utf-16"?>
    '<Vehicle xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" ID="155">
    '   <FromStatus>1</FromStatus>
    '   <ToStatus>6</ToStatus>
    '   <Master_Incident_ID>192642</Master_Incident_ID>
    '   <StationID>0</StationID>
    '</Vehicle>
    
    'Type XMLStatus
    '    UnitID As Integer
    '    FromStat As Integer
    '    ToStat As Integer
    '    CallID As Integer
    '    StatTime As Variant
    '    StationID As Integer
    'End Type
    
    On Error GoTo ParseXMLStatus_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function ParseXMLStatus")
    End If
    
    nStart = InStr(1, Message, " ID=" & Chr(34)) + Len(" ID=" & Chr(34))                '   Look for the string [ ID="] - required for v5.6 and thankfully, it's backwards-compatible :)
    nStop = InStr(nStart + 1, Message, Chr(34))
    result.UnitID = Val(Mid(Message, nStart, nStop - nStart))
    
    nStart = InStr(1, Message, "<FromStatus>") + Len("<FromStatus>")
    nStop = InStr(nStart, Message, "</FromStatus>")
    result.FromStat = Val(Mid(Message, nStart, nStop - nStart))
    
    nStart = InStr(1, Message, "<ToStatus>") + Len("<ToStatus>")
    nStop = InStr(nStart, Message, "</ToStatus>")
    result.ToStat = Val(Mid(Message, nStart, nStop - nStart))
    
    nStart = InStr(1, Message, "<Master_Incident_ID>") + Len("<Master_Incident_ID>")
    nStop = InStr(nStart, Message, "</Master_Incident_ID>")
    result.CallID = Val(Mid(Message, nStart, nStop - nStart))
    
    nStart = InStr(1, Message, "<TimeStamp>") + Len("<TimeStamp>")
    nStop = InStr(nStart, Message, "</TimeStamp>")
    If nStop = 0 Then              '   no time stamp in message for some funky reason ... needs to be fixed
        result.StatTime = Now
    Else
        result.StatTime = CDate(Mid(Message, nStart, nStop - nStart))
    End If
    
    nStart = InStr(1, Message, "<StationID>") + Len("<StationID>")
    nStop = InStr(nStart, Message, "</StationID>")
    result.StationID = Val(Mid(Message, nStart, nStop - nStart))

    ParseXMLStatus = result
    
    Exit Function
    
ParseXMLStatus_ERH:
    Call AddToDebugList("[ERROR] in Function ParseXMLStatus - " & Err.Description & " (" & Err.Number & ") - Message: >" & Message & "<")
    ParseXMLStatus = result
End Function

Private Function ParseXMLPosition(ByVal Message As String) As XMLPosition
    Dim result As XMLPosition
    Dim nStart As Integer
    Dim nStop As Integer
    Dim aMsg() As String
    
'    00:38:36 [0245][314][NP_READ_VEHICLE_POSITION                ]
'    >
'    <Vehicle ID= "164" ReadVehicleSent= "-1">
'        <Current_Lat>30229930</Current_Lat>
'        <Current_Lon>97767324</Current_Lon>
'        <Current_Location>EMS Station 1</Current_Location>
'        <Current_City>AUSTIN</Current_City>
'        <LastAVLUpdate>Apr 17 2003 00:38:33</LastAVLUpdate>
'        <Speed>0</Speed>
'        <Heading></Heading>
'        <Altitude>0</Altitude>
'    </Vehicle>
'    <
    
'    Type XMLPosition
'        UnitID As Integer
'        CurrLat As Double
'        CurrLon As Double
'        CurrLoc As String
'        CurrCity As String
'        LastAVL As Variant
'        Speed As Single
'        Heading As Single
'        Altitude As Long
'    End Type
'

    On Error GoTo ParseXMLPosition_ERH
    
    
    If Left(Message, 1) <> "<" Then                             '   required because TriTech changed the format of msg 0245 (NP_READ_VEHICLE_POSITION) in version 5.2
        
        aMsg = Split(Message, ",")
        
        result.UnitID = Val(aMsg(0))
        result.CurrLat = (Val(aMsg(1)) / 1000000) - gLatitudeAdjust
        result.CurrLon = (Val(aMsg(2)) / -1000000) - gLongitudeAdjust
            
    Else

        nStart = InStr(1, Message, Chr(34)) + 1
        nStop = InStr(nStart + 1, Message, Chr(34))
        result.UnitID = Val(Mid(Message, nStart, nStop - nStart))
        
        nStart = InStr(1, Message, "<Current_Lat>") + Len("<Current_Lat>")
        nStop = InStr(nStart, Message, "</Current_Lat>")
        result.CurrLat = (Val(Mid(Message, nStart, nStop - nStart)) / 1000000) - gLatitudeAdjust
        
        nStart = InStr(1, Message, "<Current_Lon>") + Len("<Current_Lon>")
        nStop = InStr(nStart, Message, "</Current_Lon>")
        result.CurrLon = (Val(Mid(Message, nStart, nStop - nStart)) / -1000000) - gLongitudeAdjust
        
        nStart = InStr(1, Message, "<Current_Location>") + Len("<Current_Location>")
        nStop = InStr(nStart, Message, "</Current_Location>")
        result.CurrLoc = Mid(Message, nStart, nStop - nStart)
        
        nStart = InStr(1, Message, "<Current_City>") + Len("<Current_City>")
        nStop = InStr(nStart, Message, "</Current_City>")
        result.CurrCity = Mid(Message, nStart, nStop - nStart)
        
        nStart = InStr(1, Message, "<LastAVLUpdate>") + Len("<LastAVLUpdate>")
        nStop = InStr(nStart, Message, "</LastAVLUpdate>")
        result.LastAVLUpdate = CDate(Mid(Message, nStart, nStop - nStart))
        
        nStart = InStr(1, Message, "<Speed>") + Len("<Speed>")
        nStop = InStr(nStart, Message, "</Speed>")
        result.Speed = Val(Mid(Message, nStart, nStop - nStart))
    
        nStart = InStr(1, Message, "<Heading>") + Len("<Heading>")
        nStop = InStr(nStart, Message, "</Heading>")
        result.Heading = Val(Mid(Message, nStart, nStop - nStart))
    
        nStart = InStr(1, Message, "<Altitude>") + Len("<Altitude>")
        nStop = InStr(nStart, Message, "</Altitude>")
        result.Altitude = Val(Mid(Message, nStart, nStop - nStart))

    End If

    ParseXMLPosition = result
    
    Exit Function
    
ParseXMLPosition_ERH:
    Call AddToDebugList("[ERROR] in Function ParseXMLPosition - " & Err.Description & " (" & Err.Number & ") - Message: >" & Message & "<")
    ParseXMLPosition = result
End Function

Private Sub LoadUnitList(Optional nIncID As Long = -1)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRecs As Long
    Dim n As Long
    Dim AccumLat As Double
    Dim AccumLon As Double
    Dim LatLonCount As Long
    
    Dim cJList As String
    Dim cDList As String
    
    Dim lWidth As Integer
    Dim cError As String
        
    'Private Type tVehicle
    '    UnitName As Variant
    '    VehID As Long
    '    AVLID As Long
    '    MDTID As Long
    '    RMIID As Long
    '    AVLEnabled As Boolean
    '    CurrentLat As Double
    '    CurrentLon As Double
    '    CurrentLoc As String
    '    CurrentCity As String
    '    SceneLoc As String
    '    SceneCity As String
    '    SceneLat As Double
    '    SceneLon As Double
    '    DestinationLoc As String
    '    DestinationCity As String
    '    DestinationCode As String
    '    DestinationLat As Double
    '    DestinationLon As Double
    '    Status As vcStatus
    '    QueueRow As Integer
    '    Station As String
    '    CurrentDivision As String
    '    ResourceType As String
    'End Type

    On Error GoTo ERH
    
    cSQL = "Select u.Code, v.ID, v.AVL_ID, v.AVLEnabled, " _
                        & " v.Current_Lat, v.Current_Lon, v.Current_Location, v.Current_City, " _
                        & " v.Destination_Lat, v.Destination_Lon, v.Status_ID, v.MST_MDT_ID, " _
                        & " s.Name, s.Lat, s.Lon, d.DivName, v.master_incident_id, " _
                        & " v.destination_location, v.destination_city, r.description, j.name " _
                        & " from Vehicle v Join Unit_Names u " _
                            & " on v.UnitName_ID = u.ID" _
                            & " join Stations s on v.Post_Station_ID = s.ID " _
                            & " join AgencyTypes a on a.ID = v.AgencyTypeID " _
                            & " join Division d on d.ID = v.CurrentDivision_ID " _
                            & " join Jurisdiction j on j.ID = d.JurisdictionID " _
                            & " join Resource r on v.PrimaryResource_ID = r.ID "
    
    If nIncID <= 0 Then
    
        If chkOffDuty.Value = vbUnchecked Then
            cSQL = cSQL & " where v.status_ID <> 0 "
        End If
        
        If cmbActiveAgency.Text <> "<Show All Agencies>" Then
            cSQL = cSQL & " and a.Agency_Type ='" & Trim(cmbActiveAgency.Text) & "' "
        End If
        
        If chkActiveJurisdiction.Value = vbUnchecked Then
            cJList = GetFilterList(lstActiveJurisdictions)
            If cJList <> "" Then
                cSQL = cSQL & " and j.Name in (" & cJList & ") "
            End If
        End If
        
        If chkActiveDivision.Value = vbUnchecked Then
            cDList = GetFilterList(lstActiveDivisions)
            If cDList <> "" Then
                cSQL = cSQL & " and d.DivName in (" & cDList & ") "
            End If
        End If
        
    Else
    
        cSQL = cSQL & " where v.Master_Incident_ID = " & nIncID
            
    End If
    
    cSQL = cSQL & " Order by u.Code"

    nRecs = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    AddToDebugList "[LoadUnitList] " & Str(nRecs) & " Units Found"
    
    gUnitListLength = nRecs - 1

    If nRecs > 0 Then
    
        ReDim gUnitList(0 To nRecs - 1)
    
        For n = 0 To nRecs - 1
            gUnitList(n).UnitName = aSQL(0, n)
            gUnitList(n).VehID = aSQL(1, n)
            gUnitList(n).AVLID = IIf(IsNull(aSQL(2, n)), -1, aSQL(2, n))
            gUnitList(n).AVLEnabled = aSQL(3, n)
            gUnitList(n).CurrentLat = IIf(IsNull(aSQL(4, n)), -999, aSQL(4, n) / 1000000) - gLatitudeAdjust
            gUnitList(n).CurrentLon = IIf(IsNull(aSQL(5, n)), -999, aSQL(5, n) / -1000000) - gLongitudeAdjust
            gUnitList(n).CurrentLoc = IIf(IsNull(aSQL(6, n)), "", aSQL(6, n))
            gUnitList(n).CurrentCity = IIf(IsNull(aSQL(7, n)), "", aSQL(7, n))
            gUnitList(n).DestinationLat = IIf(IsNull(aSQL(8, n)), -999, aSQL(8, n) / 1000000) - gLatitudeAdjust
            gUnitList(n).DestinationLon = IIf(IsNull(aSQL(9, n)), -999, aSQL(9, n) / -1000000) - gLongitudeAdjust
            gUnitList(n).Status = IIf(IsNull(aSQL(10, n)), 0, aSQL(10, n))
            gUnitList(n).MDTID = IIf(IsNull(aSQL(11, n)), -1, aSQL(11, n))
            gUnitList(n).Station = aSQL(12, n)
            gUnitList(n).StationLat = IIf(IsNull(aSQL(13, n)), -999, aSQL(13, n) / 1000000) - gLatitudeAdjust
            gUnitList(n).StationLon = IIf(IsNull(aSQL(14, n)), -999, aSQL(14, n) / -1000000) - gLongitudeAdjust
            gUnitList(n).CurrentDivision = IIf(IsNull(aSQL(15, n)), "", aSQL(15, n))
            gUnitList(n).RMIID = IIf(IsNull(aSQL(16, n)), -1, aSQL(16, n))
            gUnitList(n).DestinationLoc = IIf(IsNull(aSQL(17, n)), "", aSQL(17, n))
            gUnitList(n).DestinationCity = IIf(IsNull(aSQL(18, n)), "", aSQL(18, n))
            gUnitList(n).ResourceType = IIf(IsNull(aSQL(19, n)), "", aSQL(19, n))
            gUnitList(n).CurrentJurisdiction = IIf(IsNull(aSQL(6, n)), "", aSQL(20, n))
            
            If gForceAVL Then
                gUnitList(n).AVLID = gUnitList(n).VehID
                gUnitList(n).AVLEnabled = True
            End If
            
            If gForceMST Then
                gUnitList(n).MDTID = gUnitList(n).VehID
            End If
            
        Next n
        
    Else
    
        If nIncID >= 0 Then             '   we were looking for the incidents on a specific incident but we found nothing
            Call LoadUnitList           '   so load all units
        End If
    
    End If
    
    Exit Sub

ERH:
    
    AddToDebugList ("[ERROR in LoadUnitList] No.:" & Str(Err.Number) & " " & Err.Description & " @ n = " & n)
    
End Sub

Private Function RefreshUnitInfo(ByVal nUnitID As Long) As Long
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRecs As Long
    Dim n As Long
    Dim Found As Boolean
    Dim OldSectorDivision As String
    
    '   Dim lUnit As MapPointCtl.Location
    Dim nUnit As Integer
    
    Dim cError As String
    
    On Error GoTo ErrHandler:
    
    '   need to include a test here to identify sector/division changes
    
    Found = False
    n = 0
    
    '   Find UnitName in list
    
    For n = 0 To UBound(gUnitList)
        If gUnitList(n).VehID = nUnitID Then
            Found = True
            Exit For
        End If
    Next n
    
    If Found Then
        cSQL = "Select u.Code, v.ID, v.AVL_ID, v.AVLEnabled, " _
                            & " v.Current_Lat, v.Current_Lon, v.Current_Location, v.Current_City, " _
                            & " v.Destination_Lat, v.Destination_Lon, v.Status_ID, v.MST_MDT_ID, " _
                            & " s.Name, s.Lat, s.Lon, d.DivName, v.master_incident_id, " _
                            & " v.destination_location, v.destination_city, r.description, j.name " _
                            & " from Vehicle v Join Unit_Names u " _
                                & " on v.UnitName_ID = u.ID" _
                                & " join Stations s on v.Post_Station_ID = s.ID " _
                                & " join AgencyTypes a on a.ID = v.AgencyTypeID " _
                                & " join Division d on d.ID = v.CurrentDivision_ID " _
                                & " join Jurisdiction j on j.ID = d.JurisdictionID " _
                                & " join Resource r on v.PrimaryResource_ID = r.ID " _
                                & " Where v.ID = " & Str(nUnitID)
        
        If cmbActiveAgency.Text <> "<Show All Agencies>" Then
            cSQL = cSQL & " and a.Agency_Type ='" & Trim(cmbActiveAgency.Text) & "' "
        End If
        
        If chkActiveJurisdiction.Value = vbUnchecked Then
            cSQL = cSQL & " and j.Name in (" & GetFilterList(lstActiveJurisdictions) & ") "
        End If
        
        If chkActiveDivision.Value = vbUnchecked Then
            cSQL = cSQL & " and d.DivName in (" & GetFilterList(lstActiveDivisions) & ") "
        End If
    
        nRecs = bsiSQLGet(cSQL, aSQL, gConnectString)
    
        If n <= UBound(gUnitList) Then              '   vehicle ID was found in the list
        
            If nRecs > 0 Then                       '   and still in our view
                
                gUnitList(n).UnitName = aSQL(0, 0)
                gUnitList(n).AVLID = IIf(IsNull(aSQL(2, 0)), -1, aSQL(2, 0))
                gUnitList(n).AVLEnabled = aSQL(3, 0)
                gUnitList(n).CurrentLat = IIf(IsNull(aSQL(4, 0)), -999, aSQL(4, 0) / 1000000) - gLatitudeAdjust
                gUnitList(n).CurrentLon = IIf(IsNull(aSQL(5, 0)), -999, aSQL(5, 0) / -1000000) - gLongitudeAdjust
                gUnitList(n).CurrentLoc = IIf(IsNull(aSQL(6, 0)), "", aSQL(6, 0))
                gUnitList(n).CurrentCity = IIf(IsNull(aSQL(7, 0)), "", aSQL(7, 0))
                gUnitList(n).DestinationLat = IIf(IsNull(aSQL(8, 0)), -999, aSQL(8, 0) / 1000000) - gLatitudeAdjust
                gUnitList(n).DestinationLon = IIf(IsNull(aSQL(9, 0)), -999, aSQL(9, 0) / -1000000) - gLongitudeAdjust
                gUnitList(n).Status = IIf(IsNull(aSQL(10, 0)), 0, aSQL(10, 0))
                gUnitList(n).MDTID = IIf(IsNull(aSQL(11, 0)), -1, aSQL(11, 0))
                gUnitList(n).Station = aSQL(12, 0)
                gUnitList(n).StationLat = IIf(IsNull(aSQL(13, 0)), -999, aSQL(13, 0) / 1000000) - gLatitudeAdjust
                gUnitList(n).StationLon = IIf(IsNull(aSQL(14, 0)), -999, aSQL(14, 0) / -1000000) - gLongitudeAdjust
                gUnitList(n).CurrentDivision = IIf(IsNull(aSQL(15, 0)), "", aSQL(15, 0))
                gUnitList(n).RMIID = IIf(IsNull(aSQL(16, 0)), -1, aSQL(16, 0))
                gUnitList(n).DestinationLoc = IIf(IsNull(aSQL(17, 0)), "", aSQL(17, 0))
                gUnitList(n).DestinationCity = IIf(IsNull(aSQL(18, 0)), "", aSQL(18, 0))
                gUnitList(n).ResourceType = IIf(IsNull(aSQL(19, 0)), "", aSQL(19, 0))
                gUnitList(n).CurrentJurisdiction = IIf(IsNull(aSQL(20, 0)), "", aSQL(20, 0))
                
                Call RefreshUnitQueueRow(gUnitList(n))
            
            Else                        '   unit in list, but no longer in our view
            
                Call LoadUnitList       '   refresh unit queue
                Call RefreshUnitQueue
        
            End If
        
        Else
            
            If nRecs > 0 Then           '   not in the list, but now in our view
            
                '   Call LoadUnitList       '   refresh unit queue
                '   Call RefreshUnitQueue
        
            End If
            
        End If
    Else
        Call AddToDebugList("[ERROR] RefreshUnitInfo: UnitID = " & nUnitID & " NOT FOUND ")
        '   Call RefreshUnitQueue
        '   n = RefreshUnitInfo(nUnitID)
        n = -1
    End If
    
    RefreshUnitInfo = n
    
    Exit Function
    
ErrHandler:
    On Error Resume Next
    
    Call AddToDebugList("[ERROR] RefreshUnitInfo: Unit=" & gUnitList(n).UnitName & " Err#:" & Str(Err.Number) & " - " & Err.Description & " cSQL = " & cSQL)
    
End Function

Private Sub FillActiveIncidentQueue(Optional nIncNum As Long = -1)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long, M As Long
    Dim PrioColor As tPriority
    Dim Lat As Double
    Dim Lon As Double
    Dim Progress As String
    Dim nInc As Long
    
    Dim cError As String
    
    On Error GoTo FillActiveIncidentQueue_ERH
    
    Call AddToDebugList("[FillActiveIncidentQueue] Inc:" & IIf(nIncNum >= 0, Str(nIncNum), " <ALL>"))
    
    Progress = "Start of FillIncidentBrowser"

    cSQL = "select rm.ID, " _
            & " rm.Time_CallEnteredQueue, " _
            & " rm.address, " _
            & " rm.Location_Name, " _
            & " rm.city, " _
            & " rm.problem, " _
            & " rm.priority_description, " _
            & " rm.CallTaking_Performed_By, " _
            & " rm.Agency_Type, " _
            & " rm.Jurisdiction, " _
            & " rm.division, " _
            & " rm.latitude, " _
            & " rm.longitude, " _
            & " rm.Priority_Number, " _
            & " rm.master_incident_number, " _
            & " a.ID, " _
            & " rm.WhichQueue "
    cSQL = cSQL & "from Response_master_incident rm JOIN AgencyTypes a on rm.Agency_Type = a.Agency_Type " _
            & " Where (rm.WhichQueue = 'W' or rm.WhichQueue = 'A') " _
            & " and rm.Priority_Number <> 0 " _
            & " and rm.ID is not NULL " _
            & " and rm.Time_CallClosed is NULL "

    If cmbActiveAgency.Text <> "<Show All Agencies>" Then
        cSQL = cSQL & " and rm.Agency_Type ='" & Trim(cmbActiveAgency.Text) & "' "
    End If
    
    If chkActiveJurisdiction.Value = vbUnchecked Then
        cSQL = cSQL & " and rm.Jurisdiction in (" & GetFilterList(lstActiveJurisdictions) & ") "
    End If
    
    If chkActiveDivision.Value = vbUnchecked Then
        cSQL = cSQL & " and rm.Division in (" & GetFilterList(lstActiveDivisions) & ") "
    End If
    
    If nIncNum >= 0 Then
        cSQL = cSQL & " and rm.ID = " & nIncNum
    End If

    cSQL = cSQL & " ORDER BY rm.WhichQueue DESC, rm.Time_CallEnteredQueue "

    Progress = "Executing Query"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
   
    If nRows > 0 Then
        
        If nIncNum >= 0 Then
            
        Else
            ReDim gActiveList(0 To nRows - 1)
            Call SetUpActiveBrowser(nRows)
        End If
        
        For n = 0 To nRows - 1
        
            nInc = GetIncidentByIncID(aSQL(0, n), n)
        
            gActiveList(nInc).IncId = aSQL(0, n)
            gActiveList(nInc).TimeStart = IIf(IsNull(aSQL(1, n)), "", Format(aSQL(1, n), "yyyy.mm.dd hh:nn:ss"))
            gActiveList(nInc).PU_Address = IIf(IsNull(aSQL(2, n)), "", aSQL(2, n))
            gActiveList(nInc).PU_Location = IIf(IsNull(aSQL(3, n)), "", aSQL(3, n))
            gActiveList(nInc).PU_City = IIf(IsNull(aSQL(4, n)), "", aSQL(4, n))
            gActiveList(nInc).Problem = IIf(IsNull(aSQL(5, n)), "", aSQL(5, n))
            gActiveList(nInc).Priority = IIf(IsNull(aSQL(6, n)), "", aSQL(6, n))
            gActiveList(nInc).CallTaker = IIf(IsNull(aSQL(7, n)), "", aSQL(7, n))
            gActiveList(nInc).Agency = IIf(IsNull(aSQL(8, n)), "", aSQL(8, n))
            gActiveList(nInc).Jurisdiction = IIf(IsNull(aSQL(9, n)), "", aSQL(9, n))
            gActiveList(nInc).Division = IIf(IsNull(aSQL(10, n)), "", aSQL(10, n))
            gActiveList(nInc).PU_Lat = IIf(IsNull(aSQL(11, n)), 0#, aSQL(11, n)) / 1000000 - gLatitudeAdjust
            gActiveList(nInc).PU_Lon = IIf(IsNull(aSQL(12, n)), 0#, aSQL(12, n)) / 1000000 - gLatitudeAdjust
            gActiveList(nInc).Priority_Number = IIf(IsNull(aSQL(13, n)), 0, aSQL(13, n))
            gActiveList(nInc).MasterNumber = IIf(IsNull(aSQL(14, n)), "", aSQL(14, n))
            gActiveList(nInc).AgencyID = IIf(IsNull(aSQL(15, n)), 0, aSQL(15, n))
            gActiveList(nInc).Queue = aSQL(16, n)
            
            gActiveList(nInc).QueueRow = nInc + 1
            
            Call RefreshActiveQueueRow(gActiveList(nInc))
            
        Next n
        
        With flxActive
            .Col = 0
            .Row = 1
            .ColSel = .Cols - 1
        End With
        
    Else
    
        If nIncNum >= 0 Then
            Call FillActiveIncidentQueue
        End If
        
        Debug.Print cError & " - " & cSQL
        
    End If

   On Error GoTo 0
   Exit Sub

FillActiveIncidentQueue_ERH:

    Call AddToDebugList("[ERROR] in procedure FillActiveIncidentQueue of Form frmMain:" & Err.Number & ", " & Err.Description & " @ " & Progress)
    
End Sub

Private Function GetIncidentByIncID(ByVal nIncID As Long, Optional nDefault As Long = -1) As Long
    Dim n As Long
    
    GetIncidentByIncID = nDefault
    
    For n = 0 To UBound(gActiveList, 1)
        If gActiveList(n).IncId = nIncID Then
            GetIncidentByIncID = n
            Exit For
        End If
    Next n
    
End Function

Private Function GetJurisdictionList() As String
    Dim cList As String
    Dim n As Long
    
    For n = 0 To lstJurisdiction.ListCount - 1
        If lstJurisdiction.Selected(n) Then
            If Len(cList) > 0 Then cList = cList & ","
            cList = cList & "'" & lstJurisdiction.List(n) & "'"
        End If
    Next n
    
    GetJurisdictionList = cList
    
End Function

Private Function GetDivisionList() As String
    Dim cList As String
    Dim n As Long
    
    For n = 0 To lstDivision.ListCount - 1
        If lstDivision.Selected(n) Then
            If Len(cList) > 0 Then cList = cList & ","
            cList = cList & "'" & lstDivision.List(n) & "'"
        End If
    Next n
    
    GetDivisionList = cList
    
End Function

Private Sub RefreshUnitQueue()
    Dim n As Long
    
    On Error GoTo ERH
    
    Call SetUpUnitBrowser

    If gUnitListLength >= 0 Then
    
        flxUnits.Rows = UBound(gUnitList, 1) + 2
    
        For n = 0 To UBound(gUnitList)
        
            gUnitList(n).QueueRow = n + 1
            
            Call RefreshUnitQueueRow(gUnitList(n))
            
        Next n
    
    End If
    
    Exit Sub

ERH:
    
    AddToDebugList ("[ERROR in RefreshUnitQueue] No.:" & Str(Err.Number) & " " & Err.Description & " @ n = " & n)
    
End Sub

Private Sub RefreshActiveQueueRow(Incident As tIncident)
    Dim n As Long, c As Long
    Dim nRow As Long
    Dim PrioColor As tPriority
    
    On Error GoTo RefreshActiveQueueRow_ERH
    
    nRow = Incident.QueueRow
    
    With flxActive
        .TextMatrix(nRow, K_ACT_ID) = Incident.IncId
        .TextMatrix(nRow, K_ACT_QUEUE) = Incident.Queue
        .TextMatrix(nRow, K_ACT_DateTime) = Incident.TimeStart
        .TextMatrix(nRow, K_ACT_Address) = Incident.PU_Address
        .TextMatrix(nRow, K_ACT_Location) = Incident.PU_Location
        .TextMatrix(nRow, K_ACT_City) = Incident.PU_City
        .TextMatrix(nRow, K_ACT_Agency) = Incident.Agency
        .TextMatrix(nRow, K_ACT_Jurisdiction) = Incident.Jurisdiction
        .TextMatrix(nRow, K_ACT_Division) = Incident.Division
        .TextMatrix(nRow, K_ACT_Problem) = Incident.Problem
        .TextMatrix(nRow, K_ACT_Priority) = Incident.Priority
        .TextMatrix(nRow, K_ACT_CallTaker) = Incident.CallTaker
        .TextMatrix(nRow, K_ACT_Latitude) = Incident.PU_Lat
        .TextMatrix(nRow, K_ACT_Longitude) = Incident.PU_Lon
        .TextMatrix(nRow, K_ACT_PriorityNumber) = Incident.Priority_Number
        .TextMatrix(nRow, K_ACT_MasterNumber) = Incident.MasterNumber
        .TextMatrix(nRow, K_ACT_AgencyID) = Incident.AgencyID
        
        .Row = nRow
        
        PrioColor = bsiGetPriority(Incident.Priority_Number, Incident.AgencyID)
        
        For c = 0 To .Cols - 1
            .Col = c
            .CellForeColor = PrioColor.ForeColor
            .CellBackColor = PrioColor.BackColor
        Next c
    
    End With

    Exit Sub

RefreshActiveQueueRow_ERH:
    
    AddToDebugList ("[ERROR in RefreshActiveQueueRow] No.:" & Str(Err.Number) & " " & Err.Description)
    
End Sub

Private Sub RefreshUnitQueueRow(UnitList As tVehicle)
    Dim n As Long, c As Long
    Dim nRow As Long
    
    On Error GoTo ERH
    
    If UnitList.QueueRow > 0 Then
    
        nRow = UnitList.QueueRow
        
        Debug.Assert nRow < flxUnits.Rows
        
        With flxUnits
            .TextMatrix(nRow, 0) = UnitList.UnitName
            .TextMatrix(nRow, 1) = UnitList.CurrentLoc
            .TextMatrix(nRow, 2) = UnitList.CurrentCity
            .TextMatrix(nRow, 3) = UnitList.DestinationLoc
            .TextMatrix(nRow, 4) = UnitList.DestinationCity
            .TextMatrix(nRow, 5) = UnitList.CurrentJurisdiction
            .TextMatrix(nRow, 6) = UnitList.CurrentDivision
            .TextMatrix(nRow, 7) = UnitList.CurrentLat
            .TextMatrix(nRow, 8) = UnitList.CurrentLon
            .TextMatrix(nRow, 9) = UnitList.DestinationLat
            .TextMatrix(nRow, 10) = UnitList.DestinationLon
            
            .Row = nRow
            
            For c = 0 To .Cols - 1
                .Col = c
                .CellForeColor = gStatusList(UnitList.Status).ForeColor
                .CellBackColor = gStatusList(UnitList.Status).BackColor
            Next c
        
        End With

    End If

    Exit Sub

ERH:
    
    AddToDebugList ("[ERROR in RefreshUnitQueueRow] No.:" & Str(Err.Number) & " " & Err.Description & " @ n = " & nRow)
    
End Sub

Private Function GetUnitRecord(nUnitID As Long) As tVehicle
    Dim Found As Boolean
    Dim n As Long
    
    Found = False
    
    '   Find UnitName in list
    
    For n = 0 To UBound(gUnitList)
        If gUnitList(n).VehID = nUnitID Then
            Found = True
            Exit For
        End If
    Next n
    
    GetUnitRecord = gUnitList(n)

End Function

Private Function GetUnitRecordbyName(cName As String) As tVehicle
    Dim Found As Boolean
    Dim n As Long
    
    Found = False
    
    '   Find UnitName in list
    
    For n = 0 To UBound(gUnitList)
        If gUnitList(n).UnitName = cName Then
            Found = True
            Exit For
        End If
    Next n
    
    GetUnitRecordbyName = gUnitList(n)

End Function

Private Function GetUnitIndexByID(nUnitID As Long) As Long
    Dim n As Long
    
    '   Find Unit ID in list
    
    GetUnitIndexByID = -1
    
    For n = 0 To UBound(gUnitList)
        If gUnitList(n).VehID = nUnitID Then
            GetUnitIndexByID = n
            Exit For
        End If
    Next n
    
End Function

Private Function GetUnitIndexByName(cName As Long) As Long
    Dim n As Long
    
    '   Find Unit Name in list
    
    GetUnitIndexByName = -1
    
    For n = 0 To UBound(gUnitList)
        If gUnitList(n).UnitName = cName Then
            GetUnitIndexByName = n
            Exit For
        End If
    Next n
    
End Function

Private Function GetFilterList(lstTarget As ListBox) As String
    Dim cList As String
    Dim n As Long
    
    For n = 0 To lstTarget.ListCount - 1
        If lstTarget.Selected(n) Then
            cList = cList & "'" & lstTarget.List(n) & "',"
        End If
    Next n
    
    If cList <> "" Then
        cList = Left(cList, Len(cList) - 1)
    End If
    
    GetFilterList = cList
    
End Function

Public Sub AddToDebugList(sString As String)
    Dim LogItem As String
    Dim n As Long
    Dim nFile As Integer
    Dim cFile As String
    
    LogItem = Format(Now, "hh:nn:ss ") & sString
    
    If lstDebug.ListCount > 5000 Then       '   check for list overflow
        lstDebug.RemoveItem 0               '   removing '0' promotes '1' to '0'
    End If
    
    lstDebug.AddItem LogItem
    
    lstDebug.ListIndex = lstDebug.ListCount - 1
    lstDebug.Selected(lstDebug.ListIndex) = False
    
    If Dir(App.Path & "\Logs", vbDirectory) = "" Then
        MkDir App.Path & "\Logs"
    End If
    
    nFile = FreeFile
    cFile = App.Path & "\Logs\cnSimOp_" & Format(Now, "YYYY.MM.DD") & ".log"
    Open cFile For Append As #nFile
    Print #nFile, LogItem
    Close #nFile
    
End Sub

Private Sub RequestSimulationStatus()
    Dim cMsg As String
    Dim cMsgNum As String
    
    cMsgNum = Format(CN_MSG_CLASS, "0000")
    cMsg = Format(CN_MSG_SIM_STATUS, "0000") & tcpSock.LocalHostName
    
    Call SendTCPMessage(cMsgNum, cMsg)
    
End Sub

Private Sub SendTCPMessage(cMsgNum As String, cMsg As String)
    Dim cTCPMsg As String
    
    cTCPMsg = Chr(vcIP_BOM) & cMsgNum & cMsg & Chr(vcIP_EOM)
    
    tcpSock.SendData cTCPMsg
    
End Sub

Private Sub ResumeSimulation(cMasterWS As String)
    gSimulationRunning = True
    statBar.Panels.Item("Message").Text = "Simulation is RUNNING - Master Console: " & cMasterWS
    
    Call SetEnableOnAllControls(True)
    
    lblStatus.Caption = "Running"
    lblStatus.ForeColor = RGB(0, 180, 0)

End Sub

Private Sub PausedSimulation(cMasterWS As String)
    gSimulationRunning = False
    statBar.Panels.Item("Message").Text = "Simulation is PAUSED - Master Console: " & cMasterWS
    
    Call SetEnableOnAllControls(False)
    
    lblStatus.Caption = "Paused"
    lblStatus.ForeColor = RGB(255, 0, 0)

End Sub

Private Sub SetEnableOnAllControls(bSwitch As Boolean)
    
    mnuRadioMessage.Enabled = bSwitch
    mnuStopMovement.Enabled = bSwitch
    mnuResumeMovement.Enabled = bSwitch
    mnuSendANIALI.Enabled = bSwitch
    mnuSendIncident.Enabled = bSwitch
    cmdANIALI.Enabled = bSwitch
    cmdSendAniAli.Enabled = bSwitch
    cmdTransmit.Enabled = bSwitch
    cmdWAV.Enabled = bSwitch
    cmdSendCellular.Enabled = bSwitch
    
End Sub

Private Sub MasterConsoleShutdown(cMsg As String)
    Call PausedSimulation(cMsg)
End Sub

Private Sub WriteIncidentDetails(IncNum As String, Optional bUseANIALITable As Boolean = False)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim n As Integer, i As Integer, x As Integer
    Dim pRow As Long
    Dim cString As String
    Dim PrevRow As Long
    Dim aWork As Variant
    Dim atemp As Variant
    Dim cError As String
    
    Dim bSuccess As Boolean
    
    bSuccess = Not bUseANIALITable
    
    frmIncident.Cls
    
    Call frmIncident.FormPrint(1, 0, "Incident:", True)
    Call frmIncident.FormPrint(1, 1, "Address:", True)
    Call frmIncident.FormPrint(55, 1, "Apt:", True)
    Call frmIncident.FormPrint(1, 2, "Location:", True)
    Call frmIncident.FormPrint(55, 2, "City:", True)
    Call frmIncident.FormPrint(1, 3, "X-Streets:", True)
    Call frmIncident.FormPrint(1, 5, "Originator:", True)
    Call frmIncident.FormPrint(55, 5, "Phone:", True)
    Call frmIncident.FormPrint(1, 7, "Nature/Problem:", True)
    Call frmIncident.FormPrint(1, 9, "Comments:", True)
    
    If bUseANIALITable Then
        cSQL = "SELECT Master_Incident_ID from Response_ANIALI WHERE ID = " & IncNum
        
        nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
        
        If nRows > 0 Then
            If Not IsNull(aSQL(0, 0)) Then
                IncNum = aSQL(0, 0)
                bSuccess = True
            Else
                MsgBox "No incident information for this ANIALI record. Sorry.", vbOKOnly + vbInformation, "No Data Available"
            End If
        Else
            MsgBox "No incident information for this ANIALI record. Sorry.", vbOKOnly + vbInformation, "No Data Available"
        End If
        
    End If
    
    If bSuccess Then
    
        cSQL = "Select address, location_name, city, apartment, cross_street, caller_name, call_back_phone, problem, caller_location_name " _
                & " From response_master_incident " _
                & " Where ID = " & IncNum
                
        nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
        
        If nRows > 0 Then
            Call frmIncident.FormPrint(20, 0, IncNum)           '   incident number
            Call frmIncident.FormPrint(20, 1, aSQL(0, 0))       '   address
            Call frmIncident.FormPrint(65, 1, aSQL(3, 0))       '   apartment
            Call frmIncident.FormPrint(20, 2, aSQL(1, 0))       '   location
            Call frmIncident.FormPrint(65, 2, aSQL(2, 0))       '   city
            Call frmIncident.FormPrint(20, 3, aSQL(4, 0))       '   Minor x-streets
            Call frmIncident.FormPrint(20, 4, aSQL(8, 0))       '   Major x-streets (caller_location_name)
            Call frmIncident.FormPrint(20, 5, aSQL(5, 0))       '   originator
            Call frmIncident.FormPrint(65, 5, aSQL(6, 0))       '   phone number
            Call frmIncident.FormPrint(20, 7, aSQL(7, 0))       '   problem nature
        End If
    
        cSQL = "Select Additional_Comment_Group_ID, date_time, CONVERT(CHAR(1000), comment) " _
                & " From response_comments " _
                & " Where Master_Incident_ID = " & IncNum _
                & " Order by date_time "
                
        nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
        
        pRow = 10
        PrevRow = 0
        
        If nRows > 0 Then
        
            ReDim aWork(0 To 1, 0 To nRows - 1)
            
            PrevRow = aSQL(0, n)
            i = 0
            For n = 0 To nRows - 1
                If aSQL(0, n) = PrevRow Then
                    aWork(0, i) = Format(aSQL(1, n), "YYYY.MM.DD HH:NN:SS")
                    aWork(1, i) = aWork(1, i) & Trim(aSQL(2, n))
                Else
                    i = i + 1
                    PrevRow = aSQL(0, n)
                    aWork(0, i) = Format(aSQL(1, n), "YYYY.MM.DD HH:NN:SS")
                    aWork(1, i) = aWork(1, i) & Trim(aSQL(2, n))
                End If
            Next n
            
            For n = 0 To i
                
                Call frmIncident.FormPrint(3, pRow, aWork(0, n))        '   comment date/time
                
                cString = aWork(1, n)
                
                atemp = Split(cString, vbCrLf)
                
                For x = 0 To UBound(atemp)
                    cString = atemp(x)
                    Do
                        Call frmIncident.FormPrint(20, pRow, Left(cString, 80))        '   comment
                        
                        pRow = pRow + 1
                        If Len(cString) > 80 Then
                            cString = Right(cString, Len(cString) - 80)
                        Else
                            cString = ""
                        End If
                    Loop While Len(cString) > 0
                Next x
                        
            Next n
        End If
    Else
        Unload frmIncident
    End If
    
End Sub

Private Sub SendIncidentToCAD(cIncident As String)
    Dim cMsgNum As String
    Dim cHeader As String
    Dim cMsg As String
    
    cMsgNum = Format(CN_MSG_CLASS, "0000")
    cHeader = Format(CN_MSG_PASSTHROUGH, "0000") & tcpSock.LocalHostName & "|"
    
    cMsg = cHeader & Format(EV_INC, "0000") & "|" & cIncident
    
    Call SendTCPMessage(cMsgNum, cMsg)
    
    DoEvents
    
End Sub
