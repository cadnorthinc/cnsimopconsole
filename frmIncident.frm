VERSION 5.00
Begin VB.Form frmIncident 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   3420
      TabIndex        =   0
      Top             =   2700
      Width           =   1215
   End
End
Attribute VB_Name = "frmIncident"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub FormPrint(x As Long, y As Long, txtString, Optional lBold As Boolean = False)

    With frmIncident

        .AutoRedraw = True
        .ScaleMode = vbCharacters
        .Font.Name = "ARIAL"
        .Font.Size = 10
        .FontBold = lBold
        
        .CurrentX = x
        .CurrentY = y
        
        If .CurrentY > .ScaleHeight - 1 Then
            .Height = .Height + 240
            .ScaleMode = vbTwips
            .cmdClose.Top = .ScaleHeight - .cmdClose.Height - 60
            .cmdClose.Left = .ScaleWidth - .cmdClose.Width - 60
            .ScaleMode = vbCharacters
        End If
    
    End With
    
    Debug.Print txtString

    frmIncident.Print txtString
        
End Sub

Private Sub cmdClose_Click()
    Unload frmIncident
End Sub

Private Sub Form_Load()
    Dim formTop As Long
    Dim formLeft As Long
    
    formTop = Val(bsiGetSettings("SystemDefaults", "IncFormTop", "-1", gSystemDirectory & "\Simulator.INI"))
    frmIncident.Top = IIf(formTop > 0, formTop, (Screen.Height / 2) - (frmIncident.Height / 2))

    formLeft = Val(bsiGetSettings("SystemDefaults", "IncFormLeft", "-1", gSystemDirectory & "\Simulator.INI"))
    frmIncident.Left = IIf(formLeft > 0, formLeft, (Screen.Width / 2) - (frmIncident.Width / 2))
    
    frmIncident.Width = fmMain.Width
    frmIncident.Height = fmMain.Height
    
    frmIncident.cmdClose.Top = frmIncident.ScaleHeight - frmIncident.cmdClose.Height - 60
    frmIncident.cmdClose.Left = frmIncident.ScaleWidth - frmIncident.cmdClose.Width - 60

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim Test As Boolean
    
    Test = bsiPutSettings("SystemDefaults", "IncFormTop", frmIncident.Top, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "IncFormLeft", frmIncident.Left, gSystemDirectory & "\Simulator.INI")

End Sub

