Attribute VB_Name = "Startup"
Public Const INIPATH = "Q:\Tritech\VisiCAD\Data\System"
Public Const C_INIPATH = "C:\Tritech\VisiCAD\Data\System"
Public Const Q_INIPATH = "Q:\Tritech\VisiCAD\Data\System"

Public Const K_FLX_ROLL = 0
Public Const K_FLX_ID = 1
Public Const K_FLX_DateTime = 2
Public Const K_FLX_Address = 3
Public Const K_FLX_City = 4
Public Const K_FLX_Quad = 5
Public Const K_FLX_Problem = 6
Public Const K_FLX_Unit = 7
Public Const K_FLX_UnitType = 8
Public Const K_FLX_CallTaker = 9
Public Const K_FLX_Destination = 10
Public Const K_FLX_Assigned = 11
Public Const K_FLX_Enroute = 12
Public Const K_FLX_AtScene = 13
Public Const K_FLX_Transport = 14
Public Const K_FLX_AtDest = 15
Public Const K_FLX_AvailAtDest = 16
Public Const K_FLX_Available = 17
Public Const K_FLX_Latitude = 18
Public Const K_FLX_Longitude = 19
Public Const K_FLX_ForeColor = 20
Public Const K_FLX_BackColor = 21

Public Const K_ACT_ID = 0
Public Const K_ACT_QUEUE = 1
Public Const K_ACT_DateTime = 2
Public Const K_ACT_Address = 3
Public Const K_ACT_Location = 4
Public Const K_ACT_City = 5
Public Const K_ACT_Problem = 6
Public Const K_ACT_Priority = 7
Public Const K_ACT_CallTaker = 8
Public Const K_ACT_Agency = 9
Public Const K_ACT_Jurisdiction = 10
Public Const K_ACT_Division = 11
Public Const K_ACT_Latitude = 12
Public Const K_ACT_Longitude = 13
Public Const K_ACT_PriorityNumber = 14
Public Const K_ACT_MasterNumber = 15
Public Const K_ACT_AgencyID = 16

Public Const K_UNIT_UNIT = 0
Public Const K_UNIT_CURRLOC = 1
Public Const K_UNIT_CURRCITY = 2
Public Const K_UNIT_DESTLOC = 3
Public Const K_UNIT_DESTCITY = 4
Public Const K_UNIT_JUR = 5
Public Const K_UNIT_DIV = 6
Public Const K_UNIT_CURRLAT = 7
Public Const K_UNIT_CURRLON = 8
Public Const K_UNIT_DESTLAT = 9
Public Const K_UNIT_DESTLON = 10

Public Const EV_AVL = 1                '   Event AVL Update
Public Const EV_STAT = 2               '   Event STATUS Update
Public Const EV_INC = 3                '   Event INCIDENT Creation
Public Const EV_TRANS = 4              '   Event Transporting (Destination) report
Public Const EV_RADIO = 5              '   Event Radio Message from Unit
Public Const EV_E911 = 6               '   Send ANIALI Message using the RMI.ID
Public Const EV_INIT_OFF = 7           '   Begin Intitialization - Units Off Duty
Public Const EV_INIT_ON = 8            '   Step 2 Intitialization - Units ON Duty
Public Const EV_INIT_INC = 9           '   Step 3 Intitialization - Link Incidents and Units
Public Const EV_INIT_DONE = 10         '   Step 4 Intitialization Complete
Public Const EV_ANIALI = 11            '   Event Send ANIALI message using the ANIALI table ID

Public KeyInfo As tLicenseInfo                  '   all info relating to the license

Public gSystemDirectory As String
Public gIPCServer As String
Public gSimScriptDir As String
Public gSoundByteDir As String

Public gLatitudeAdjust As Double
Public gLongitudeAdjust As Double
Public gOffsetFile As String

Public gUseESRIResources As Boolean
Public gESRINetworkDataset As String
Public gESRINetworkCost As String
Public gAVLUpdateLimit As Integer
Public gUseOneWayRestrictions As Boolean

Public gConnectString As String

Public gDetailedDebugging As Boolean

Sub Main()
    
    On Error GoTo StartupDAMMIT
    
    Dim bContinue As Boolean
    Dim aCommandArgs() As String
    Dim nArgc As Integer
    Dim aArgv() As String
    Dim aArg() As String
    Dim Test As Boolean
    Dim cShareLoc As String
    Dim n As Integer
    
    bContinue = False
    
    Call GetCommandlineArgs(Command$, nArgc, aArgv)
    
    If nArgc >= 0 Then                '   there are commandline arguments
        For n = 0 To UBound(aArgv, 1)
            aArg = Split(aArgv(n), "=")
            If Trim(UCase(aArg(0))) = "SHARE" Then
                cShareLoc = Trim(UCase(aArg(1)))
                If cShareLoc = "Q:" Then
                    gSystemDirectory = Q_INIPATH
                ElseIf cShareLoc = "C:" Then
                    gSystemDirectory = C_INIPATH
                '    Else                                        '   incorrect parameter specified on command line
                '        Call MsgBox("TriTech directory structure location missing." & vbCrLf & "Specify SHARE=Q: or SHARE=C: as a command line parameter." & vbCrLf & "Simulator cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP")
                End If
            ElseIf Trim(UCase(aArg(0))) = "IPCSERVER" Then                                            '   incorrect parameter specified on command line
                gIPCServer = Trim(UCase(aArg(1)))
            End If
        Next n
    Else
        gSystemDirectory = Q_INIPATH                    '   default to Q: share
    End If
    
    If Dir(gSystemDirectory & "\System.INI") <> "" And gIPCServer <> "" Then
        bContinue = True
    Else                                            '   incorrect parameter specified on command line
        Call MsgBox("Please specify TriTech Share and IPCServer location on command line." _
                & vbCrLf & " ... SHARE=Q: or SHARE=C: and" _
                & vbCrLf & " ... IPCServer=<IP address> or IPCServer=<Host Name>." & vbCrLf _
                & vbCrLf & "cnSimOp Console cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP")
    End If
    
    If bContinue Then
        Load frmSplash
        frmSplash.Show
        
        If cnValidateLicense(KeyInfo) Then
            Load fmMain
            Unload frmSplash
        Else
            Unload frmSplash
        End If
    End If
    
    Exit Sub
    
StartupDAMMIT:
    Call MsgBox("TriTech directory structure location missing." & vbCrLf & "Specify SHARE=Q: or SHARE=C: as a command line parameter." & vbCrLf & "Simulator cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP")
End Sub

Private Sub GetCommandlineArgs(cCommandline As String, ByRef nArgc As Integer, ByRef aArgv() As String)
    Dim aParameters() As String
    Dim n As Integer
    
    aParameters = Split(cCommandline, " ")
    
    nArgc = 0
    
    For n = 0 To UBound(aParameters, 1)
        If Len(aParameters(n)) > 0 Then
            ReDim Preserve aArgv(0 To nArgc)
            aArgv(nArgc) = aParameters(n)
            nArgc = nArgc + 1
        End If
    Next n
    
End Sub
